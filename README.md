# Build for Tempnetz device

## Requirements

Debian Jessie armhf chroot

Schroot configuration /etc/schroot/schroot.conf

## Build

### Step 1

Invoke chroot
```sudo schroot -c ${CHROOT_NAME}```

### Step 2

Build Debian package. This will build the library and place it in {lib} directory and then package into the Debian binary located one directory back.

```debuild -us -uc```



 

#include "tnetportal.h"

#define CHART_PLOT_BACKGROUND_N_COLOR     QColor(40,40,40)
#define CHART_AXIS_N_COLOR                QColor(Qt::white)
#define CHART_AXIS_LABEL_N_COLOR          QColor(40,40,40)
#define CHART_GRID_N_COLOR                QColor(140,140,140)
#define CHART_SUBGRID_N_COLOR             QColor(80,80,80)
#define CHART_TEMPERATURE_PLOT_N_COLOR    QColor(Qt::yellow)
#define CHART_POINT_BRUSH_N_COLOR         QColor(Qt::white)
#define CHART_REFERENCE_PLOT_N_COLOR      QColor(Qt::green)
#define CHART_DIFFERENCE_PLOT_N_COLOR     QColor(Qt::magenta)
#define CHART_ALARM1_PLOT_N_COLOR         QColor(Qt::cyan)
#define CHART_ALARM2_PLOT_N_COLOR         QColor(Qt::red)
#define CHART_TITLE_N_COLOR               QColor(Qt::white)
#define CHART_LEGEND_BRUSH_N_COLOR        QColor(100,100,100,220)
#define CHART_LEGEND_TEXT_N_COLOR         QColor(Qt::white)


#define CHART_WIDGET_BACKGROUND_P_COLOR   QColor(Qt::white)
#define CHART_PLOT_BACKGROUND_P_COLOR     QColor(Qt::white)
#define CHART_AXIS_P_COLOR                QColor(Qt::black)
#define CHART_AXIS_LABEL_P_COLOR          QColor(Qt::black)
#define CHART_GRID_P_COLOR                QColor(Qt::gray)
#define CHART_SUBGRID_P_COLOR             QColor(Qt::gray)
#define CHART_TEMPERATURE_PLOT_P_COLOR    QColor(Qt::black)
#define CHART_POINT_BRUSH_P_COLOR         QColor(Qt::black)
#define CHART_REFERENCE_PLOT_P_COLOR      QColor(Qt::green)
#define CHART_DIFFERENCE_PLOT_P_COLOR     QColor(Qt::magenta)
#define CHART_ALARM1_PLOT_P_COLOR         QColor(Qt::cyan)
#define CHART_ALARM2_PLOT_P_COLOR         QColor(Qt::red)
#define CHART_TITLE_P_COLOR               QColor(Qt::black)
#define CHART_LEGEND_BRUSH_P_COLOR        QColor(100,100,100,150)
#define CHART_LEGEND_TEXT_P_COLOR         QColor(Qt::black)


/**
 * @brief TgChart::TgChart
 * @param parent
 */
TgChart::TgChart(QWidget * parent, bool degreesCelsius, QSet<ChartItemVisibility> * visibleItems) :
    QCustomPlot(parent),
    chartIsActive(false),
    zoomEnabled(false),
    panEnabled(false),
    toolTipEnabled(false),
    degreesInCelsius(degreesCelsius)
{
    mRubberBand = new QRubberBand(QRubberBand::Rectangle, this);
    connect(this, SIGNAL(mouseMove(QMouseEvent*)), this, SLOT(dataPointHint(QMouseEvent*)));
    connect(this, SIGNAL(mousePress(QMouseEvent*)), this, SLOT(chartPressed()));

    //setInteractions(QCP::iRangeDrag /*| QCP::iRangeZoom | QCP::iSelectAxes*/ | QCP::iSelectLegend | /*QCP::iSelectPlottables*/);

    QLinearGradient plotGradient;
    plotGradient.setStart(0, 0);
    plotGradient.setFinalStop(0, 350);
    plotGradient.setColorAt(1, QColor(150,150,150));
    plotGradient.setColorAt(0, QColor(80,80,80));
    setBackground(plotGradient);
    axisRect()->setBackground(CHART_PLOT_BACKGROUND_N_COLOR);

    legendVisible = visibleItems->contains(cvLegend) ? true: false;
    refPlotVisible = visibleItems->contains(cvRef) ? true: false;
    diffPlotVisible = visibleItems->contains(cvDiff) ? true: false;
    alarm1PlotVisible = visibleItems->contains(cvA1) ? true: false;
    alarm2PlotVisible = visibleItems->contains(cvA2) ? true: false;
    hGridVisible = visibleItems->contains(cvHGrid) ? true: false;
    vGridVisible = visibleItems->contains(cvVGrid) ? true: false;

}

/**
 * @brief TgChart::~TgChart
 */
TgChart::~TgChart()
{
    delete mRubberBand;
}

/**
 * @brief TgChart::mousePressEvent
 * @param event
 */
void TgChart::mousePressEvent(QMouseEvent * event)
{
    if (zoomEnabled)
    {
        if ( event->button() == Qt::MiddleButton)
        {
            mOrigin = QPoint(event->pos().x(),axisRect()->topLeft().y());
            mRubberBand->setGeometry(QRect(mOrigin,
                                           QSize()));
            mRubberBand->show();
        }
    }
    QCustomPlot::mousePressEvent(event);
}

/**
 * @brief TgChart::mouseMoveEvent
 * @param event
 */
void TgChart::mouseMoveEvent(QMouseEvent * event)
{
    if (zoomEnabled)
    {
        if (mRubberBand->isVisible())
        {
            mRubberBand->setGeometry(QRect(mOrigin, QPoint(event->pos().x(),axisRect()->bottomRight().y())).normalized());
        }
    }
    QCustomPlot::mouseMoveEvent(event);
}

/**
 * @brief TgChart::mouseReleaseEvent
 * @param event
 */
void TgChart::mouseReleaseEvent(QMouseEvent * event)
{
    if (zoomEnabled && mRubberBand->isVisible())
    {
        QRect zoomRect = mRubberBand->geometry();

        xAxis->setRange(xAxis->pixelToCoord( zoomRect.topLeft().x()), xAxis->pixelToCoord( zoomRect.bottomRight().x()));

        mRubberBand->hide();
        replot();
    }

    QCustomPlot::mouseReleaseEvent(event);
}

/**
 * @brief TgChart::dataPointHint
 * @param event
 */
void TgChart::dataPointHint(QMouseEvent *event)
{
    if(!toolTipEnabled)
    {
        setToolTip(QString(""));
        return;
    }

    // x point on the chart in chart coordinates
    int chartCursorX = this->xAxis->pixelToCoord(event->pos().x());

    // now find the nearest axis point
    const QCPDataMap *dataMap = this->graph(TG_PLOT_TEMPERATURE)->data();
    QMap<double, QCPData>::const_iterator i = dataMap->lowerBound(chartCursorX);
    //qDebug() << i.key() << ": " << i.value().key << ": " << i.value().value << endl;


    setToolTip(QString("%1:\n%2 %3C").arg(this->graph(TG_PLOT_TEMPERATURE)->name()).arg(i.value().value).arg(G_UNIT_DEGREES));
}

/**
 * @brief TgChart::chartPressed
 */
void TgChart::chartPressed()
{
    axisRect()->setRangeDrag(Qt::Horizontal);
}

/**
 * @brief TgChart::grabGesture
 * @param gestures
 * @details Parent should pass done when getsure event triggered, filter here what we need
 */
void TgChart::grabGestures(const QList<Qt::GestureType> &gestures)
{
    foreach (Qt::GestureType gesture, gestures) {
        //qDebug() << "Gesture grab " << gesture;
        grabGesture(gesture);
    }
}

/**
 * @brief TgChart::setChartActive
 * @param active
 */
void TgChart::setChartActive(bool active)
{
    chartIsActive = active;
    if (chartIsActive)
        TG_DEBUG("Set chart active");
    else
        TG_DEBUG("Set chart inactive");
}

/**
 * @brief TgChart::setZoomEnabled
 * @param active
 */
void TgChart::setZoomEnabled(bool enabled)
{
    zoomEnabled = enabled;

    if (zoomEnabled)
       TG_DEBUG("Enable zoom");
    else
        TG_DEBUG("Disable zoom");

}

/**
 * @brief TgChart::setPanEnabled
 * @param enabled
 */
void TgChart::setPanEnabled(bool enabled)
{
    panEnabled = enabled;

    if (panEnabled)
        TG_DEBUG("Enable pan");
    else
        TG_DEBUG("Disable pan");
}

/**
 * @brief TgChart::setToolTipEnabled
 * @param enabled
 */
void TgChart::setToolTipEnabled(bool enabled)
{
    toolTipEnabled = enabled;

    if (toolTipEnabled)
        TG_DEBUG("Enable tool tip");
    else
        TG_DEBUG("Disable tool tip");
}

/**
 * @brief TgChart::event
 * @param event
 * @return
 */
bool TgChart::event(QEvent *event)
{

    if (event->type() == QEvent::Gesture) {
        QGestureEvent* gEvent = static_cast<QGestureEvent*>(event);

        if (QGesture *pan = gEvent->gesture(Qt::PanGesture))
            panTriggered(static_cast<QPanGesture *>(pan));
        if (QGesture *pinch = gEvent->gesture(Qt::PinchGesture))
            pinchTriggered(static_cast<QPinchGesture *>(pinch));

        return true;
    }
    return QCustomPlot::event(event);
}

/**
 * @brief TgChart::panTriggered
 */
void TgChart::panTriggered(QPanGesture *)
{
    TG_DEBUG("Pan gesture triggered");
}

/**
 * @brief TgChart::pinchTriggered
 */
void TgChart::pinchTriggered(QPinchGesture *)
{
    TG_DEBUG("Pinch gesture triggered");
}


/**
 * @brief TgChart::clearChart
 * @details Clear plots axis, legends and titles
 */
void TgChart::clearChart()
{
    // clear plots
    clearPlottables();

    // clear axis from axis rect
    axisRect()->removeAxis(xAxis);
    axisRect()->removeAxis(yAxis);

    // clear legend
    legend->clear();

    // clear title element
    TG_DEBUG("Total layout elements = " << plotLayout()->elementCount());
    plotLayout()->clear();
}

/**
 * @brief TgChart::refresh
 */
void TgChart::refresh()
{

}

/**
 * @brief TgChart::toggleTempPlotVisibility
 */
void TgChart::setTempPlotVisibility(bool visible)
{
    tempPlotVisible = visible;
    sensorTemp->setVisible(visible);
    refresh();
}

/**
 * @brief TgChart::toggleRefPlotVisibility
 */
void TgChart::setRefPlotVisibility(bool visible)
{
    if (sensorRefPos == 0)
        return;

    refPlotVisible = visible;
    refSensorTemp->setVisible(refPlotVisible);
    refresh();
}

/**
 * @brief TgChart::toggleDiffPlotVisibility
 */
void TgChart::setDiffPlotVisibility(bool visible)
{
    if (sensorRefPos == 0)
        return;

    diffPlotVisible = visible;
    diffSensorTemp->setVisible(diffPlotVisible);
    refresh();
}

/**
 * @brief TgChart::toggleAlarmsPlotVisibility
 */
void TgChart::setAlarm1PlotVisibility(bool visible)
{
    alarm1PlotVisible = visible;
    sensorA1->setVisible(alarm1PlotVisible);
    refresh();
}

/**
 * @brief TgChart::toggleAlarm2PlotVisibility
 */
void TgChart::setAlarm2PlotVisibility(bool visible)
{
    alarm2PlotVisible = visible;
    sensorA2->setVisible(alarm2PlotVisible);
    refresh();
}

/**
 * @brief TgChart::toggleHGridVisibility
 */
void TgChart::setHGridVisibility(bool visible)
{
    hGridVisible = visible;
    yAxis->grid()->setVisible(hGridVisible);
    refresh();
}

/**
 * @brief TgChart::toggleVGridVisibility
 */
void TgChart::setVGridVisibility(bool visible)
{
    vGridVisible = visible;
    xAxis->grid()->setVisible(vGridVisible);
    refresh();
}


/**
 * @brief TgChart::toggleLegendVisibility
 */
void TgChart::setLegendVisibility(bool visible)
{
    legendVisible = visible;
    legend->setVisible(legendVisible);
    refresh();
}

void TgChart::setPrintColors(bool enabled)
{
    if (enabled){
        setBackground(QBrush(CHART_PLOT_BACKGROUND_P_COLOR));
        axisRect()->setBackground(CHART_PLOT_BACKGROUND_P_COLOR);

        // legend
        legend->setBrush(QBrush(CHART_LEGEND_BRUSH_P_COLOR));
        legend->setTextColor(CHART_LEGEND_TEXT_P_COLOR);

        // temperature plot line
        sensorTemp->setPen(QPen(CHART_TEMPERATURE_PLOT_P_COLOR));

        // plot title text
        QCPPlotTitle * plotTitle = static_cast<QCPPlotTitle *>(plotLayout()->elementAt(0));
        plotTitle->setTextColor(CHART_TITLE_P_COLOR);

        // datetime axis
        xAxis->setLabelColor(CHART_AXIS_LABEL_P_COLOR);
        xAxis->setBasePen(QPen(CHART_AXIS_P_COLOR, 1));
        xAxis->setTickPen(QPen(CHART_AXIS_P_COLOR, 1));
        xAxis->setSubTickPen(QPen(CHART_AXIS_P_COLOR, 1));
        xAxis->setTickLabelColor(CHART_AXIS_P_COLOR);
        xAxis->grid()->setPen(QPen(CHART_GRID_P_COLOR, 1, Qt::DotLine));
        xAxis->grid()->setSubGridPen(QPen(CHART_GRID_P_COLOR, 1, Qt::DotLine));
        xAxis->grid()->setSubGridVisible(false);

        // temperature axis
        yAxis->setLabelColor(CHART_AXIS_LABEL_P_COLOR);
        yAxis->setBasePen(QPen(CHART_AXIS_P_COLOR, 1));
        yAxis->setTickPen(QPen(CHART_AXIS_P_COLOR, 1));
        yAxis->setSubTickPen(QPen(CHART_AXIS_P_COLOR, 1));
        yAxis->setTickLabelColor(CHART_AXIS_P_COLOR);
        yAxis->grid()->setPen(QPen(CHART_GRID_P_COLOR, 1, Qt::DotLine));
        yAxis->grid()->setSubGridPen(QPen(CHART_GRID_P_COLOR, 1, Qt::DotLine));
        yAxis->grid()->setSubGridVisible(false);

    } else {
        // legend
        legend->setBrush(QBrush(CHART_LEGEND_BRUSH_N_COLOR));
        legend->setTextColor(CHART_LEGEND_TEXT_N_COLOR);

        QLinearGradient plotGradient;
        plotGradient.setStart(0, 0);
        plotGradient.setFinalStop(0, 350);
        plotGradient.setColorAt(1, QColor(150,150,150));
        plotGradient.setColorAt(0, QColor(80,80,80));
        setBackground(plotGradient);
        axisRect()->setBackground(CHART_PLOT_BACKGROUND_N_COLOR);

        // temperature plot line
        sensorTemp->setPen(QPen(CHART_TEMPERATURE_PLOT_N_COLOR));

        // plot title text
        QCPPlotTitle * plotTitle = static_cast<QCPPlotTitle *>(plotLayout()->elementAt(0));
        plotTitle->setTextColor(CHART_TITLE_N_COLOR);

        // datetime axis
        xAxis->setLabelColor(CHART_AXIS_LABEL_N_COLOR);
        xAxis->setBasePen(QPen(CHART_AXIS_N_COLOR, 1));
        xAxis->setTickPen(QPen(CHART_AXIS_N_COLOR, 1));
        xAxis->setSubTickPen(QPen(CHART_AXIS_N_COLOR, 1));
        xAxis->setTickLabelColor(CHART_AXIS_N_COLOR);
        xAxis->grid()->setPen(QPen(CHART_GRID_N_COLOR, 1, Qt::DotLine));
        xAxis->grid()->setSubGridPen(QPen(CHART_GRID_N_COLOR, 1, Qt::DotLine));
        xAxis->grid()->setSubGridVisible(false);

        // temperature axis
        yAxis->setLabelColor(CHART_AXIS_LABEL_N_COLOR);
        yAxis->setBasePen(QPen(CHART_AXIS_N_COLOR, 1));
        yAxis->setTickPen(QPen(CHART_AXIS_N_COLOR, 1));
        yAxis->setSubTickPen(QPen(CHART_AXIS_N_COLOR, 1));
        yAxis->setTickLabelColor(CHART_AXIS_N_COLOR);
        yAxis->grid()->setPen(QPen(CHART_GRID_N_COLOR, 1, Qt::DotLine));
        yAxis->grid()->setSubGridPen(QPen(CHART_GRID_N_COLOR, 1, Qt::DotLine));
        yAxis->grid()->setSubGridVisible(false);
    }

    refresh();
}

bool TgChart::saveAsImage(const char *format, const QString &fileName, int _width, int _height, double _scale, int _quality)
{
    return saveRastered(fileName,_width,_height,_scale,format,_quality);
}












/********************************************
 *          Log chart: Data from database
 *******************************************/




TgLogChart::TgLogChart(QWidget *parent, bool degreesCelsius, QSet<ChartItemVisibility> *visibleItems) :
    TgChart(parent,degreesCelsius,visibleItems)

{
    dloader = 0;
    threadTimeoutTimer.setInterval(10000);
    QObject::connect(&threadTimeoutTimer, SIGNAL(timeout()), this, SLOT(threadTimerExpired()));
    alarmSearchTimer.setInterval(10);
    QObject::connect(&alarmSearchTimer, SIGNAL(timeout()), this, SLOT(alarmSearchTimerExpired()));


    zoomEnabled = false;
    panEnabled = false;
    tempPlotVisible = true;
    toolTipEnabled = false;
    alarmSearchActive = false;

    QFont font;
    QPen graphPen;

    // temperature plots
    sensorTemp = addGraph(xAxis, yAxis);
    sensorTemp->setName("");
    sensorTemp->setLineStyle(QCPGraph::lsLine);
    sensorTemp->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssNone));
    graphPen.setColor(CHART_TEMPERATURE_PLOT_N_COLOR);
    graphPen.setWidthF(1);
    sensorTemp->setPen(graphPen);

    // alarm 1
    sensorA1 = addGraph(xAxis, yAxis);
    sensorA1->setName("Alarm 1");
    sensorA1->setLineStyle(QCPGraph::lsLine);
    sensorA1->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssNone));
    graphPen.setColor(CHART_ALARM1_PLOT_N_COLOR);
    graphPen.setWidthF(0.5);
    sensorA1->setPen(graphPen);

    // alarm 2
    sensorA2 = addGraph(xAxis, yAxis);
    sensorA2->setName("Alarm 2");
    sensorA2->setLineStyle(QCPGraph::lsLine);
    sensorA2->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssNone));
    graphPen.setColor(CHART_ALARM2_PLOT_N_COLOR);
    graphPen.setWidthF(0.5);
    sensorA2->setPen(graphPen);

    // reference sensor temp
    refSensorTemp = addGraph(xAxis, yAxis);
    refSensorTemp->setName("");
    refSensorTemp->setLineStyle(QCPGraph::lsLine);
    refSensorTemp->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssNone));
    graphPen.setColor(CHART_REFERENCE_PLOT_N_COLOR);
    graphPen.setWidthF(0.5);
    refSensorTemp->setPen(graphPen);

    // difference temp
    diffSensorTemp = addGraph(xAxis, yAxis);
    diffSensorTemp->setName("Difference");
    diffSensorTemp->setLineStyle(QCPGraph::lsLine);
    diffSensorTemp->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssNone));
    graphPen.setColor(CHART_DIFFERENCE_PLOT_N_COLOR);
    graphPen.setWidthF(0.5);
    diffSensorTemp->setPen(graphPen);

    // add title
    plotLayout()->insertRow(0);
    plotTitle = new QCPPlotTitle(this,"");
    plotTitle->setTextColor(CHART_TITLE_N_COLOR);
    font.setPointSize(12);
    plotTitle->setFont(font);
    plotLayout()->addElement(0, 0, plotTitle);

    // axisRect is at row 2


    // legend
    axisRect()->insetLayout()->setInsetAlignment(0,Qt::AlignLeft|Qt::AlignTop);
    legend->setBrush(CHART_LEGEND_BRUSH_N_COLOR);
    legend->font().setPointSize(10);
    legend->setSelectedFont(legend->font());
    //legend->setSelectableParts(QCPLegend::spItems); // legend box shall not be selectable, only legend items


    axisRect()->setAutoMargins(QCP::msBottom | QCP::msLeft | QCP::msTop);
    QMargins margins;
    margins.setRight(50);
    axisRect()->setMargins(margins);

    // datetime axis
    font.setPointSize(12);
    xAxis->setLabelColor(CHART_AXIS_LABEL_N_COLOR);
    xAxis->setLabel("Date/time");
    xAxis->setLabelFont(font);
    xAxis->setAutoTicks(true);
    xAxis->setAutoTickCount(5);
    xAxis->setTickLabelSide(QCPAxis::lsOutside);
    xAxis->setBasePen(QPen(CHART_AXIS_N_COLOR, 1));
    xAxis->setTickPen(QPen(CHART_AXIS_N_COLOR, 1));
    xAxis->setSubTickPen(QPen(CHART_AXIS_N_COLOR, 1));
    xAxis->setTickLabelColor(CHART_AXIS_N_COLOR);
    xAxis->grid()->setPen(QPen(CHART_GRID_N_COLOR, 1, Qt::DotLine));
    xAxis->grid()->setSubGridPen(QPen(CHART_SUBGRID_N_COLOR, 1, Qt::DotLine));
    xAxis->grid()->setSubGridVisible(true);
    xAxis->grid()->setZeroLinePen(Qt::NoPen);
    xAxis->setUpperEnding(QCPLineEnding::esNone);
    xAxis->setTickLabelType(QCPAxis::ltDateTime);
    xAxis->setDateTimeFormat("ddd dd/MM/yy\nHH:mm:ss");

    yAxis->setLabelFont(font);
    yAxis->setLabelColor(CHART_AXIS_LABEL_N_COLOR);
    yAxis->setBasePen(QPen(CHART_AXIS_N_COLOR, 1));
    yAxis->setTickPen(QPen(CHART_AXIS_N_COLOR, 1));
    yAxis->setSubTickPen(QPen(CHART_AXIS_N_COLOR, 1));
    yAxis->setTickLabelColor(CHART_AXIS_N_COLOR);
    yAxis->grid()->setPen(QPen(CHART_GRID_N_COLOR, 1, Qt::DotLine));
    yAxis->grid()->setSubGridPen(QPen(CHART_SUBGRID_N_COLOR, 1, Qt::DotLine));
    yAxis->grid()->setSubGridVisible(true);
    yAxis->grid()->setZeroLinePen(Qt::NoPen);
    yAxis->setUpperEnding(QCPLineEnding::esNone);
    if (degreesInCelsius){
        yAxis->setLabel(QString("Temperature (%1C)").arg(G_UNIT_DEGREES));
        yAxis->setRangeLower(-10);
        yAxis->setRangeUpper(50);
    } else {
        yAxis->setLabel(QString("Temperature (%1F)").arg(G_UNIT_DEGREES));
        yAxis->setRangeLower(CelciusToFahrheit(-10));
        yAxis->setRangeUpper(CelciusToFahrheit(50));
    }

    TG_DEBUG("Reference plot visible = " << refPlotVisible);
    TG_DEBUG("Difference plot visible = " << diffPlotVisible);
    TG_DEBUG("Alarm 1 plot visible = " << alarm1PlotVisible);
    TG_DEBUG("Alarm 2 plot visible = " << alarm2PlotVisible);
    TG_DEBUG("Legend visible = " << legendVisible);
    TG_DEBUG("HGrid visible = " << hGridVisible);
    TG_DEBUG("VGrid visible = " << vGridVisible);

    sensorTemp->setVisible(tempPlotVisible);
    refSensorTemp->setVisible(refPlotVisible);
    diffSensorTemp->setVisible(diffPlotVisible);
    sensorA1->setVisible(alarm1PlotVisible);
    sensorA2->setVisible(alarm2PlotVisible);
    xAxis->grid()->setVisible(vGridVisible);
    yAxis->grid()->setVisible(hGridVisible);
    legend->setVisible(legendVisible);

    TG_DEBUG("Log Chart constructed");
}

/**
 * @brief TgLogChart::~TgLogChart
 */
TgLogChart::~TgLogChart()
{
    try {
        if (dloader)
            dloader->deleteLater();

        if (workerThread)
            workerThread->deleteLater();

    } catch(TgException &e) {
        TG_DEBUG("Log Chart destructing exception = " << e.what());
    }
    TG_DEBUG("Log Chart destructed");
}

/**
 * @brief TgLogChart::queryFinished
 * @param result
 * @param foundAlarms
 * @details worker thread does the long query and emits finished to this slot
 */
void TgLogChart::queryFinished(bool result, bool foundAlarms)
{
    threadTimeoutTimer.stop();

    if (result) {

        // have active search and we found alarm(s) in page just loaded
        if (alarmSearchActive) {
            if (!foundAlarms) {          
                alarmSearchTimer.start(30);
                dloader->deleteLater();
                workerThread->quit();
                dloader = NULL;
                workerThread = NULL;
                return;
            }
        }

        // get vector points from query and fill the plots
        const QVector<double>& temp = dloader->getTemps();
        const QVector<double>& datetime = dloader->getDt();
        const QVector<double>& a1 = dloader->getA1();
        const QVector<double>& a2 = dloader->getA2();
        const QVector<double>& refTemp = dloader->getRefTemp();
        const QVector<double>& diffTemp = dloader->getDiffTemp();

        double topBound = dloader->getTopBound();
        double bottomBound = dloader->getBottomBound();

        sensorTemp->setData(datetime,temp);
        sensorA1->setData(datetime,a1);
        sensorA2->setData(datetime,a2);

        // if diff plot
        if (sensorRefPos != 0 && sensorRefPos != sensorPos) {
            refSensorTemp->setData(datetime,refTemp);
            diffSensorTemp->setData(datetime,diffTemp);
        }

        // set axis ranges
        yAxis->setRange(bottomBound-10.0, topBound+10.0);

        totalPoints = datetime.count();
        TG_DEBUG("Total points for chart = " << totalPoints);
        // set datetime axis to 10% latest
        /*if (totalPoints > 1000){
            int percent10 = (int)(0.1 * (float)totalPoints);
            xAxis->setRange(datetime.at(totalPoints-1-percent10), datetime.at(datetime.count()-1));
        } else*/
        xAxis->setRange(datetime.at(0), datetime.at(datetime.count()-1));


        // set full range used by @ref zoomOut/zoomReturn
        fullRange.x1 = datetime.at(0);
        fullRange.x2 = datetime.at(datetime.count()-1);

        const Session_t session = tgGateway->getSession();

        // set graph names
        sensorTemp->setName(sensorName);
        refSensorTemp->setName(sensorRefName);

        // set title
        if (sensorRefPos != 0 && sensorRefPos != sensorPos)
            plotTitle->setText(QString("Session: %1\t\tSensor %2: %3\tRef Sensor %4: %5").arg(
                                                                  session.sessionnumber).arg(
                                                                  sensorPos).arg(
                                                                  sensorName).arg(
                                                                  sensorRefPos).arg(
                                                                  sensorRefName));
        else
            plotTitle->setText(QString("Session: %1\t\tSensor %2: %3").arg(
                                                       session.sessionname).arg(
                                                       sensorPos).arg(
                                                       sensorName));

        // axis label
        xAxis->setLabel(QString("Date/time  page %1/%2").arg(currentFileIndex+1).arg(totalFiles));

        refresh();

        // plots copy the vectors in setData, now clear it from the dbWorker else there are 2 copies in memory
        dloader->clearData();
    }

    // emit signal to gui
    if (alarmSearchActive) {
        alarmSearchActive = false;
        emit chartReady(result,true);
    } else
        emit chartReady(result,false);

    dloader->deleteLater();
    workerThread->quit();
    dloader = NULL;
    workerThread = NULL;

    TG_DEBUG("Query finished");
}

/**
 * @brief TgLogChart::loadData
 */
void TgLogChart::loadData()
{
    const QString& rootPath = tgGateway->getDevicePath();
    QString filePath(QString(TNET_DEVICE_SESSION_DATA_FILE).arg(rootPath, currentSession, currentFile));
    QFile f(filePath);

    workerThread = new QThread;
    dloader = new DataLoader(0,
                             sensorCount,
                             sensorPos,
                             sensorRefPos,
                             f.fileName(),
                             alarmSearchActive,
                             degreesInCelsius);
    dloader->moveToThread(workerThread);

    QObject::connect(workerThread, SIGNAL(started()), dloader, SLOT(loadData()));
    QObject::connect(dloader, SIGNAL(dataLoaded(bool,bool)), this, SLOT(queryFinished(bool,bool)));
    QObject::connect(workerThread, SIGNAL(finished()), workerThread, SLOT(deleteLater()));

    threadTimeoutTimer.start();
    workerThread->start();
}

bool TgLogChart::filterDataFiles()
{
    const QString rootPath = tgGateway->getDevicePath();
    QDir sessionDir(QString(TNET_DEVICE_SESSION_DIR).arg(rootPath, currentSession));

    // directory exists ?
    if (!sessionDir.exists()) {
        TG_WARNING("Session " << sessionDir.dirName() << " does not exist");
        return false;
    }

    // sort ascending order i.e. oldest data file at top, tdata.log at bottom

    QStringList filters;
    filters << "tdata.log.*";
    sessionDir.setNameFilters(filters);
    sessionDir.setSorting(QDir::Name);
    logFilesEntry.clear();

    for (int i=0; i< sessionDir.entryList().count(); i++)
        logFilesEntry.append(sessionDir.entryList().at(i));
    logFilesEntry.append("tdata.log");

    totalFiles = logFilesEntry.count();
    TG_DEBUG("Total data files = " << totalFiles << " for session " << currentSession);

    return true;
}

int TgLogChart::getFileIndex(const QString &filename)
{
    for (int i=0; i<logFilesEntry.count(); i++){
        if (logFilesEntry.at(i) == filename)
            return i;
    }

    return -1;
}

/**
 * @brief TgLogChart::loadSensorData
 * @param session
 * @param totalSensors
 * @param sensorPos
 * @param sensorName
 * @param sensorRefPos
 * @param sensorRefName
 * @param loadLatest
 * @return
 */
bool TgLogChart::loadSensorData(QString session,
                                quint16 totalSensors,
                                quint16 _sensorPos,
                                QString _sensorName,
                                quint16 _sensorRefPos,
                                QString _sensorRefName,
                                bool loadLatestData,
                                QString fileToLoad)
{
    if (threadTimeoutTimer.isActive()) {
        TG_WARNING("Busy with transaction");
        return false;
    }

    currentSession = session;
    sensorPos = _sensorPos;
    sensorName = _sensorName;
    sensorRefPos = _sensorRefPos;
    sensorRefName = _sensorRefName;
    sensorCount = totalSensors;
    const QString rootPath = tgGateway->getDevicePath();

    TG_DEBUG("Session " << session);
    TG_DEBUG("Total sensors " << sensorCount);
    TG_DEBUG("Sensor pos " << sensorPos);
    TG_DEBUG("Sensor name " << sensorName);
    TG_DEBUG("Ref sensor pos " << sensorRefPos);
    TG_DEBUG("Ref sensor name " << sensorRefName);

    filterDataFiles();
    QFile dataFile;

    // load latest
    if (loadLatestData) {
        // file exists ?
        QString filepath(QString(TNET_DEVICE_SESSION_DATA_FILE).arg(rootPath, currentSession, "tdata.log"));
        dataFile.setFileName(filepath);
        if (!dataFile.exists()){
            TG_WARNING("Data file " << dataFile.fileName() << " does not exist");
            return false;
        }

        currentFile = QString("tdata.log");
        currentFileIndex = totalFiles-1;
        TG_DEBUG("CurrentFileIndex = " << currentFileIndex);

    // keep same page i.e. currentFile
    // but load different sensor
    } else {
        int fileindex = getFileIndex(fileToLoad);
        if (fileindex != -1){
            currentFileIndex = fileindex;
            currentFile = fileToLoad;
        }

        TG_DEBUG("Current file name = " << currentFile << " current file index = " << currentFileIndex);

        // file exists ?
        QString filepath(QString(TNET_DEVICE_SESSION_DATA_FILE).arg(rootPath, currentSession, currentFile));
        dataFile.setFileName(filepath);
        if (!dataFile.exists()){
            TG_WARNING("Data file " << dataFile.fileName() << " does not exist");
            return false;
        }
    }

    // start executing in thread
    loadData();
    return true;
}

/**
 * @brief TgLogChart::loadLatest
 * @return
 */
bool TgLogChart::loadLatest()
{
    if (threadTimeoutTimer.isActive()) {
        TG_WARNING("Busy with transaction");
        return false;
    }

    filterDataFiles();
    TG_DEBUG("Current file name = " << currentFile << " current file index = " << currentFileIndex);

    currentFileIndex = totalFiles-1;
    currentFile = logFilesEntry.at(currentFileIndex);

    TG_DEBUG("Next file name = " << currentFile << " next file index = " << currentFileIndex);

    loadData();
    return true;
}

/**
 * @brief TgLogChart::loadOldest
 * @return
 */
bool TgLogChart::loadOldest()
{
    if (threadTimeoutTimer.isActive()) {
        TG_WARNING("Busy with transaction");
        return false;
    }

    filterDataFiles();
    TG_DEBUG("Current file name = " << currentFile << " current file index = " << currentFileIndex);
    currentFileIndex = 0;
    currentFile = logFilesEntry.at(0);
    TG_DEBUG("Next file name = " << currentFile << " next file index = " << currentFileIndex);

    loadData();
    return true;
}

/**
 * @brief TgLogChart::panLeft
 * @details Move to older data
 */
bool TgLogChart::panLeft()
{

    if (threadTimeoutTimer.isActive()) {
        TG_WARNING("Busy with transaction");
        return false;
    }

    filterDataFiles();

    TG_DEBUG("Current file name = " << currentFile << " current file index = " << currentFileIndex);
    if (currentFileIndex == 0) {
        if (alarmSearchActive) {
            alarmSearchActive = false;
            TG_INFO("End of data in alarm search");
            emit chartReady(true, false);
            return true;
        } else {
            TG_INFO("End of data");
            return true;
        }
    } else {
        currentFileIndex--;
        currentFile = logFilesEntry.at(currentFileIndex);
        TG_DEBUG("Next file name = " << currentFile << " next file index = " << currentFileIndex);
    }

    loadData();
    return true;
}

/**
 * @brief TgLogChart::panRight
 * @details Move to more recent data
 */
bool TgLogChart::panRight()
{
    if (threadTimeoutTimer.isActive()) {
        TG_WARNING("Busy with transaction");
        return false;
    }

    filterDataFiles();

    TG_DEBUG("Current file name = " << currentFile << " current file index = " << currentFileIndex);

    if (currentFileIndex == totalFiles-1) {     
        TG_INFO("Beginning of data");
        return true;
    } else {
        currentFileIndex++;
        currentFile = logFilesEntry.at(currentFileIndex);
        TG_DEBUG("Next file name = " << currentFile << " next file index = " << currentFileIndex);
    }

    loadData();
    return true;
}

/**
 * @brief TgLogChart::zoomOut
 * @details Zoom out to show full range on x axis
 */
void TgLogChart::zoomOut()
{
    // save the view for later use when @zoomReturn is called
    viewPortRange.x1 = xAxis->range().lower;
    viewPortRange.x2 = xAxis->range().upper;

    // now expand the range fully (fullRange set after loading data)
    xAxis->setRange(fullRange.x1, fullRange.x2);

    // replot
    refresh();
}

/**
 * @brief TgLogChart::zoomReturn
 * @details Zoom in back to last viewport
 */
void TgLogChart::zoomReturn()
{
    // restore
    xAxis->setRange(viewPortRange.x1, viewPortRange.x2);

    // replot
    refresh();
}

/**
 * @brief TgLogChart::searchAlarms
 * @details Search alarms by paging through the data and checking for alarms events
 */
void TgLogChart::searchAlarms()
{
    alarmSearchActive = true;
    alarmSearchStartFile = currentFile;
    panLeft();
}

QString TgLogChart::getCurrentFile()
{
    return currentFile;
}

/**
 * @brief TgLogChart::refresh
 */
void TgLogChart::refresh()
{
    replot();
}

/**
 * @brief TgLogChart::threadTimerExpired
 * @details Thread watchdog timer
 */
void TgLogChart::threadTimerExpired()
{
    threadTimeoutTimer.stop();
    TG_ERROR("Thread timer expired");
    dloader->deleteLater();
    emit queryFinished(false,false);
}

/**
 * @brief TgLogChart::alarmSearchTimerExpired
 */
void TgLogChart::alarmSearchTimerExpired()
{
    alarmSearchTimer.stop();
    alarmSearchActive = true;
    panLeft();
}

/**
 * @brief DataLoader::DataLoader
 * @param parent
 */
DataLoader::DataLoader(QObject *parent, quint16 total, quint16 position, quint16 refPosition, QString filename, bool checkForAlarms, bool degreesCelsius) :
    QObject(parent),
    totalSensors(total),
    sensorPos(position),
    sensorRefPos(refPosition),
    fileToLoad(filename),
    doAlarmSearch(checkForAlarms),
    topBound(0),
    bottomBound(0),
    degreesInCelsius(degreesCelsius)
{
    TG_DEBUG("Dataloader constructed");
}

/**
 * @brief DataLoader::~DataLoader
 */
DataLoader::~DataLoader()
{
    TG_DEBUG("Dataloader destructed");
}

/**
 * @brief DataLoader::clearData
 */
void DataLoader::clearData()
{
    temp.clear();
    dt.clear();
    a1.clear();
    a2.clear();
    refTemp.clear();
    diffTemp.clear();
}

/**
 * @brief DataLoader::loadData
 */
void DataLoader::loadData()
{
    // load data
    temp.clear();
    dt.clear();
    a1.clear();
    a2.clear();
    refTemp.clear();
    diffTemp.clear();

    // file already checked for existence prior to calling this
    QFile dataFile(fileToLoad);
    TG_DEBUG("File to load "<< dataFile.fileName().toLatin1().data());

    bool alarmFound = false;

    // open file
    if (!dataFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
        TG_WARNING("Unable to open " << dataFile.fileName());
        emit dataLoaded(false,false);
        //emit finished();
        return;
    }

    try {
        qulonglong dtSample;
        double  tempSample, a1Sample, a2Sample, diffSample, refSample;
        // process file
        while(!dataFile.atEnd()) {

            QString line = QString(dataFile.readLine());

            // ensure there is a comma delimiter in the line
            if (!line.contains(",")){
                TG_DEBUG("Line has no , delimeter");
                continue;
            }

            QStringList tokens = line.split(',');

            // verify there should be 1+4*networkSize tokens
            if (tokens.count() != 1 + 4*totalSensors) {
                TG_DEBUG("Tokens line count incorrect got " << tokens.count() << " expecting " << 1+4*totalSensors);
                continue;
            }

            // datetime as utc number
            // rough validation should be between 2017 and 2050
            dtSample = tokens.at(0).toULongLong();
            /*if (dtSample < 1483142400 || dtSample > 2556057600)
                continue;*/
            dt.append((double)dtSample);

            // index in sensor grouping
            // 0 - temp
            // 1 - a1
            // 2 - a2
            // 3 - status

            // temperature should be between -55.0 and 125.0 with 200 being fault
            if (degreesInCelsius){
                tempSample = (double)tokens.at((sensorPos-1)*4 + 1).toFloat();
                if (tempSample < -55.0 || tempSample > 200.0)
                    continue;
            }else{
                tempSample = CelciusToFahrheit((double)tokens.at((sensorPos-1)*4 + 1).toFloat());
                if (tempSample < -67.0 || tempSample > 392.0)
                    continue;
            }


            // a1 should be between -55 and 125
            if (degreesInCelsius){
                a1Sample = (double)tokens.at((sensorPos-1)*4 + 2).toInt();
                if (a1Sample < -55 || a1Sample > 125)
                    continue;
            } else {
                a1Sample = CelciusToFahrheit((double)tokens.at((sensorPos-1)*4 + 2).toInt());
                if (a1Sample < -67 || a1Sample > 257)
                    continue;
            }

            // a2 should be between -55 and 125
            if (degreesInCelsius){
                a2Sample = (double)tokens.at((sensorPos-1)*4 + 3).toInt();
                if (a2Sample < -55 || a2Sample > 125)
                    continue;
            } else {
                a2Sample = CelciusToFahrheit((double)tokens.at((sensorPos-1)*4 + 3).toInt());
                if (a2Sample < -67 || a2Sample > 257)
                    continue;
            }

            // top bound
            if ( tempSample > topBound)
                topBound = tempSample;

            if ( a1Sample > topBound)
                topBound = a1Sample;

            if ( a2Sample > topBound)
                topBound = a2Sample;

            // bottom bound
            if ( tempSample < bottomBound)
                bottomBound = tempSample;

            if ( a1Sample < bottomBound)
                bottomBound = a1Sample;

            if ( a2Sample < bottomBound)
                bottomBound = a2Sample;

            // add to chart vectors
            temp.append(tempSample);
            a1.append(a1Sample);
            a2.append(a2Sample);

            // does sensor reference another ?
            if (sensorRefPos != 0 && sensorPos != sensorRefPos) {
                if (degreesInCelsius){
                    refSample = (double)tokens.at((sensorRefPos-1)*4 + 1).toFloat();
                    diffSample = tempSample - refSample;
                } else {
                    refSample = CelciusToFahrheit((double)tokens.at((sensorRefPos-1)*4 + 1).toFloat());
                    // first calculate diff in celsius
                    double t, r;
                    t = (double)tokens.at((sensorPos-1)*4 + 1).toFloat();
                    r = (double)tokens.at((sensorRefPos-1)*4 + 1).toFloat();
                    diffSample = CelciusToFahrheit(t-r);
                }



                // top bound
                if ( refSample > topBound)
                    topBound = refSample;

                if ( diffSample > topBound)
                    topBound = diffSample;

                // bottom bound
                if ( refSample < bottomBound)
                    bottomBound = refSample;

                if ( diffSample < bottomBound)
                    bottomBound = diffSample;

                if (doAlarmSearch) {
                    if (diffSample >= a2Sample || diffSample >= a1Sample){
                        alarmFound = true;
                        //TG_DEBUG("Found diff alarm ( temp=" << tempSample << " ref=" << refSample << " diff=" << diffSample << " a1=" << a1Sample << " a2=" << a2Sample  );
                    }
                }

                refTemp.append(refSample);
                diffTemp.append(diffSample);

            } else {
                if (doAlarmSearch) {
                    if (tempSample >= a2Sample || tempSample >= a1Sample){
                        alarmFound = true;
                        //TG_DEBUG("Found alarm (temp=" << tempSample << " a1=" << a1Sample << " a2=" << a2Sample  );
                    }
                }
            }
        }

        dataFile.close();

    } catch (TgException &e) {
        TG_WARNING("Exception occured e=" << e.what());
        dataFile.close();
        emit dataLoaded(false,false);
        //emit finished();
        return;
    }

    // don't plot if there are no points
    if (temp.count() == 0) {
        TG_WARNING("No points to plot");
        emit dataLoaded(false,false);
        //emit finished();
        return;
    } else
        TG_DEBUG("Loaded samples = " << temp.count());

    emit dataLoaded(true,alarmFound);
    //emit finished();
}



















/********************************************
 *          Stream chart: Data real-time stream
 *******************************************/

/**
 * @brief TgStreamChart::addNewStreamPoint
 * @param point
 */
TgStreamChart::TgStreamChart(QWidget *parent, bool degreesCelsius,QSet<ChartItemVisibility> *visibleItems) :
    TgChart(parent,degreesCelsius,visibleItems),
    have100Points(false)
{
    zoomEnabled = false;
    panEnabled = false;
    toolTipEnabled = false;
    tempPlotVisible = true;



    QFont font;
    QPen graphPen;


    // temperature plots
    sensorTemp = addGraph(xAxis, yAxis);
    sensorTemp->setName("");
    sensorTemp->setLineStyle(QCPGraph::lsLine);
    sensorTemp->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, CHART_TEMPERATURE_PLOT_N_COLOR, CHART_POINT_BRUSH_N_COLOR, 5));
    graphPen.setColor(CHART_TEMPERATURE_PLOT_N_COLOR);
    graphPen.setWidthF(1);
    sensorTemp->setPen(graphPen);

    // alarm 1
    sensorA1 = addGraph(xAxis, yAxis);
    sensorA1->setName("Alarm 1");
    sensorA1->setLineStyle(QCPGraph::lsLine);
    sensorA1->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssNone));
    graphPen.setColor(CHART_ALARM1_PLOT_N_COLOR);
    graphPen.setWidthF(0.5);
    sensorA1->setPen(graphPen);

    // alarm 2
    sensorA2 = addGraph(xAxis, yAxis);
    sensorA2->setName("Alarm 2");
    sensorA2->setLineStyle(QCPGraph::lsLine);
    sensorA2->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssNone));
    graphPen.setColor(CHART_ALARM2_PLOT_N_COLOR);
    graphPen.setWidthF(0.5);
    sensorA2->setPen(graphPen);


    // reference sensor temp
    refSensorTemp = addGraph(xAxis, yAxis);
    refSensorTemp->setName("");
    refSensorTemp->setLineStyle(QCPGraph::lsLine);
    refSensorTemp->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, CHART_REFERENCE_PLOT_N_COLOR, CHART_POINT_BRUSH_N_COLOR, 2));
    graphPen.setColor(CHART_REFERENCE_PLOT_N_COLOR);
    graphPen.setWidthF(0.5);
    refSensorTemp->setPen(graphPen);

    // difference temp
    diffSensorTemp = addGraph(xAxis, yAxis);
    diffSensorTemp->setName("Difference");
    diffSensorTemp->setLineStyle(QCPGraph::lsLine);
    diffSensorTemp->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, CHART_DIFFERENCE_PLOT_N_COLOR, CHART_POINT_BRUSH_N_COLOR, 2));
    graphPen.setColor(CHART_DIFFERENCE_PLOT_N_COLOR);
    graphPen.setWidthF(0.5);
    diffSensorTemp->setPen(graphPen);

    // add title
    plotLayout()->insertRow(0);
    plotTitle = new QCPPlotTitle(this,"");
    plotTitle->setTextColor(CHART_TITLE_N_COLOR);
    font.setPointSize(12);
    plotTitle->setFont(font);
    plotLayout()->addElement(0, 0, plotTitle);


    // axisRect is at row 2


    // legend
    legend->setBrush(CHART_LEGEND_BRUSH_N_COLOR);
    axisRect()->insetLayout()->setInsetAlignment(0,Qt::AlignLeft|Qt::AlignTop);
    legend->font().setPointSize(10);
    legend->setSelectedFont(legend->font());
    legend->setSelectableParts(QCPLegend::spItems); // legend box shall not be selectable, only legend items

    axisRect()->setAutoMargins(QCP::msBottom | QCP::msLeft | QCP::msTop);
    QMargins margins;
    margins.setRight(50);
    axisRect()->setMargins(margins);

    // datetime axis
    font.setPointSize(12);
    xAxis->setLabelColor(CHART_AXIS_LABEL_N_COLOR);
    xAxis->setLabel("Date/time");
    xAxis->setLabelFont(font);
    xAxis->setAutoTicks(false);
    xAxis->setTickLabelSide(QCPAxis::lsOutside);
    xAxis->setBasePen(QPen(CHART_AXIS_N_COLOR, 1));
    xAxis->setTickPen(QPen(CHART_AXIS_N_COLOR, 1));
    xAxis->setSubTickPen(QPen(CHART_AXIS_N_COLOR, 1));
    xAxis->setTickLabelColor(CHART_AXIS_N_COLOR);
    xAxis->grid()->setPen(QPen(CHART_GRID_N_COLOR, 1, Qt::DotLine));
    xAxis->grid()->setSubGridPen(QPen(CHART_SUBGRID_N_COLOR, 1, Qt::DotLine));
    xAxis->grid()->setSubGridVisible(true);
    xAxis->grid()->setZeroLinePen(Qt::NoPen);
    xAxis->setUpperEnding(QCPLineEnding::esNone);
    xAxis->setTickLabelType(QCPAxis::ltDateTime);
    xAxis->setDateTimeFormat("ddd dd/MM/yy\nHH:mm:ss");

    // dynamic changing of axis ticks
    connect(xAxis, SIGNAL(ticksRequest()), this, SLOT(ticksRequestSlot()));

    yAxis->setLabelFont(font);
    yAxis->setLabelColor(CHART_AXIS_LABEL_N_COLOR);
    yAxis->setBasePen(QPen(CHART_AXIS_N_COLOR, 1));
    yAxis->setTickPen(QPen(CHART_AXIS_N_COLOR, 1));
    yAxis->setSubTickPen(QPen(CHART_AXIS_N_COLOR, 1));
    yAxis->setTickLabelColor(CHART_AXIS_N_COLOR);
    yAxis->grid()->setPen(QPen(CHART_GRID_N_COLOR, 1, Qt::DotLine));
    yAxis->grid()->setSubGridPen(QPen(CHART_SUBGRID_N_COLOR, 1, Qt::DotLine));
    yAxis->grid()->setSubGridVisible(true);
    yAxis->grid()->setZeroLinePen(Qt::NoPen);
    yAxis->grid()->setVisible(false);
    yAxis->setUpperEnding(QCPLineEnding::esNone);
    if (degreesInCelsius){
        yAxis->setLabel(QString("Temperature (%1C)").arg(G_UNIT_DEGREES));
        yAxis->setRangeLower(-10);
        yAxis->setRangeUpper(50);
    } else {
        yAxis->setLabel(QString("Temperature (%1F)").arg(G_UNIT_DEGREES));
        yAxis->setRangeLower(CelciusToFahrheit(-10));
        yAxis->setRangeUpper(CelciusToFahrheit(50));
    }


    TG_DEBUG("Reference plot visible = " << refPlotVisible);
    TG_DEBUG("Difference plot visible = " << diffPlotVisible);
    TG_DEBUG("Alarm 1 plot visible = " << alarm1PlotVisible);
    TG_DEBUG("Alarm 2 plot visible = " << alarm2PlotVisible);
    TG_DEBUG("Legend visible = " << legendVisible);
    TG_DEBUG("HGrid visible = " << hGridVisible);
    TG_DEBUG("VGrid visible = " << vGridVisible);

    sensorTemp->setVisible(tempPlotVisible);
    if (sensorRefPos != 0 && sensorRefPos != sensorPos) {
        refSensorTemp->setVisible(refPlotVisible);
        diffSensorTemp->setVisible(diffPlotVisible);
    }

    sensorA1->setVisible(alarm1PlotVisible);
    sensorA2->setVisible(alarm2PlotVisible);
    xAxis->grid()->setVisible(vGridVisible);
    yAxis->grid()->setVisible(hGridVisible);
    legend->setVisible(legendVisible);

    TG_DEBUG("Stream Chart constructed");
}

/**
 * @brief TgStreamChart::~TgStreamChart
 */
TgStreamChart::~TgStreamChart()
{
    TG_DEBUG("Stream Chart destructed");
}

/**
 * @brief TgStreamChart::setGateway
 * @param gateway
 */
void TgStreamChart::setGateway(TgGateway *gateway)
{
    tgGateway = gateway;
    QObject::connect(tgGateway, SIGNAL(streamReceived(TgEvStreamClass)), this, SLOT(onGatewayStreamReceived(TgEvStreamClass)));
}

/**
 * @brief TgStreamChart::newSensorGraph
 * @param graphOptions
 */
void TgStreamChart::newSensorGraph(SensorGraphData_t graphOptions)
{
   sensorPos = graphOptions.sensorPos;
   sensorRefPos = graphOptions.sensorRefPos;

   const Session_t session = tgGateway->getSession();

   // set plot names
   sensorTemp->setName(graphOptions.sensorAlias);

   if (sensorRefPos != 0 && sensorRefPos != sensorPos) {
        refSensorTemp->setName(graphOptions.sensorRefAlias);
        plotTitle->setText(QString("Sensor %1: %2    Sensor %3: %4").arg(sensorPos).arg(
                                                                     graphOptions.sensorAlias).arg(
                                                                     sensorRefPos).arg(
                                                                     graphOptions.sensorRefAlias));

   } else {
       plotTitle->setText(QString("Sensor %1: %2").arg(sensorPos).arg(
                                                       graphOptions.sensorAlias));
   }
}

/**
 * @brief TgStreamChart::plotLoadData
 */
void TgStreamChart::refresh(void)
{
    QVector<double> temp;
    QVector<double> a1;
    QVector<double> a2;
    QVector<double> dt;
    QVector<double> refTemp;
    QVector<double> diffTemp;

    double bottomBound = 0;
    double topBound = 0;

    // NOTE: streamPoints is Qlist<QPair<qint64, QVector<TempPoint_t> > >
    //       where first is the datetime as epoch and second is the vector of sensor points for the date time

    const QList<QPair<qint64,QVector<TempPoint_t> > >& streamPoints = tgGateway->getStreamPoints();

    for (int i=0; i<streamPoints.count(); i++) {
        dt.prepend((double)streamPoints.at(i).first);

        double tempVal, a1Val, a2Val;

        if (degreesInCelsius){
            tempVal = (double)streamPoints.at(i).second.at(sensorPos-1).temp;
            a1Val = (double)streamPoints.at(i).second.at(sensorPos-1).a1;
            a2Val = (double)streamPoints.at(i).second.at(sensorPos-1).a2;
        } else {
            tempVal = CelciusToFahrheit((double)streamPoints.at(i).second.at(sensorPos-1).temp);
            a1Val = CelciusToFahrheit((double)streamPoints.at(i).second.at(sensorPos-1).a1);
            a2Val = CelciusToFahrheit((double)streamPoints.at(i).second.at(sensorPos-1).a2);
        }

        temp.prepend(tempVal);
        a1.prepend(a1Val);
        a2.prepend(a2Val);

        // top bound
        if (tempVal > topBound)
            topBound = tempVal;

        if (a1Val > topBound)
            topBound = a1Val;

        if ( a2Val > topBound)
            topBound = a2Val;

        // bottom bound
        if (tempVal < bottomBound)
            bottomBound = tempVal;

        if (a1Val < bottomBound)
            bottomBound = a1Val;

        if (a2Val < bottomBound)
            bottomBound = a2Val;

        if (sensorRefPos != 0) {

            double refVal, diffVal;

            if (degreesInCelsius){
                refVal = (double)streamPoints.at(i).second.at(sensorRefPos-1).temp;
                diffVal = tempVal - refVal;
            } else {
                refVal = CelciusToFahrheit((double)streamPoints.at(i).second.at(sensorRefPos-1).temp);

                // first calculate diff in celsius
                double t, r;
                t = (double)streamPoints.at(i).second.at(sensorPos-1).temp;
                r = (double)streamPoints.at(i).second.at(sensorRefPos-1).temp;
                diffVal = CelciusToFahrheit((t - r));
            }



            refTemp.prepend(refVal);
            diffTemp.prepend(diffVal);

            // top bound
            if (refVal > topBound)
                topBound = refVal;

            if (diffVal > topBound)
                topBound = diffVal;

            // bottom bound
            if (refVal < bottomBound)
                bottomBound = refVal;

            if (diffVal < bottomBound)
                bottomBound = diffVal;
        }
    }

    sensorTemp->setData(dt,temp);
    sensorA1->setData(dt,a1);
    sensorA2->setData(dt,a2);

    if (sensorRefPos != 0 && sensorRefPos != sensorPos) {
        refSensorTemp->setData(dt,refTemp); 
        diffSensorTemp->setData(dt,diffTemp);
    }

    xAxis->setRange(dt.at(0), dt.at(dt.count()-1));
    // set temp range dynamically
    yAxis->setRange(bottomBound-10.0, topBound+10.0);

    // if points are greater than 100, don't draw the point
    if (dt.count() >= 100) {
        have100Points = true;
        sensorTemp->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssNone));
        refSensorTemp->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssNone));
        diffSensorTemp->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssNone));
    }

    replot(rpImmediate);
    TG_DEBUG("Stream chart replot points = " << dt.count());
}

void TgStreamChart::setPrintColors(bool enabled)
{


    if (enabled){

        // have < 100 points, draw point
        if (!have100Points){
            // temp
            sensorTemp->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle,
                                                    CHART_TEMPERATURE_PLOT_P_COLOR,
                                                    CHART_POINT_BRUSH_P_COLOR, 5));
            // ref temp
            refSensorTemp->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle,
                                                    CHART_REFERENCE_PLOT_P_COLOR,
                                                    CHART_POINT_BRUSH_P_COLOR, 5));
            // diff temp
            diffSensorTemp->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle,
                                                    CHART_DIFFERENCE_PLOT_P_COLOR,
                                                    CHART_POINT_BRUSH_P_COLOR, 5));

        } else {
            sensorTemp->setScatterStyle((QCPScatterStyle::ssNone));
            refSensorTemp->setScatterStyle((QCPScatterStyle::ssNone));
            diffSensorTemp->setScatterStyle((QCPScatterStyle::ssNone));
        }

    } else {

        if (!have100Points){
            // temp
            sensorTemp->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle,
                                                    CHART_TEMPERATURE_PLOT_N_COLOR,
                                                    CHART_POINT_BRUSH_N_COLOR, 5));
            // ref temp
            refSensorTemp->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle,
                                                    CHART_REFERENCE_PLOT_N_COLOR,
                                                    CHART_POINT_BRUSH_N_COLOR, 5));
            // diff temp
            diffSensorTemp->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle,
                                                    CHART_DIFFERENCE_PLOT_N_COLOR,
                                                    CHART_POINT_BRUSH_N_COLOR, 5));

        } else {
            sensorTemp->setScatterStyle((QCPScatterStyle::ssNone));
            refSensorTemp->setScatterStyle((QCPScatterStyle::ssNone));
            diffSensorTemp->setScatterStyle((QCPScatterStyle::ssNone));

        }
    }

    // call parent to set most of items and importantly refresh
    TgChart::setPrintColors(enabled);
}



/**
 * @brief TgStreamChart::onGatewayStreamReceived
 */
void TgStreamChart::onGatewayStreamReceived(TgEvStreamClass type)
{
    if (type != ecTemp)
        return;

    // is plot showing ?
    if (chartIsActive)
        refresh();
}

/**
 * @brief TgStreamChart::ticksRequestSlot
 */
void TgStreamChart::ticksRequestSlot()
{
    QVector<double> v;

    // get list of stream points from gateway
     const QList<QPair<qint64,QVector<TempPoint_t> > >& streamPoints = tgGateway->getStreamPoints();

    v.append(streamPoints.last().first);
    v.append(streamPoints.first().first);
    xAxis->setTickVector(v);
}


#include "tnetportal.h"

/**
 * @brief TgConfig::load
 * @details Load configuration options from config file
 */
void TgConfig::load()
{
    QFile configFile(QString(TG_CONFIG_FILE_PATH).arg(rootPath));
    if (!configFile.exists()) {
        loadDefaults();
        return;
    }

    QSettings config(QString(TG_CONFIG_FILE_PATH).arg(rootPath), QSettings::IniFormat,0);
    config.beginGroup("logging");
    options.insert(cfgLogPath , config.value(QString("path")).toString());
    options.insert(cfgLogLevel , config.value(QString("level")).toInt());
    config.endGroup();
}

/**
 * @brief TgConfig::getOption
 * @details Get config value for config key
 * @param option
 * @return
 */
const QVariant &TgConfig::getOption(ConfigOptions option)
{
    return options[option];
}

/**
 * @brief TgConfig::loadDefaults
 * @details Load default configuration options
 */
void TgConfig::loadDefaults()
{
    options.clear();

    options.insert(cfgLogPath , QString(TG_CONFIG_DEFAULT_LOG_PATH).arg(rootPath));
    options.insert(cfgLogLevel , TG_CONFIG_DEFAULT_LOG_LEVEL);

    // dump defaults to file
    QSettings config(QString(TG_CONFIG_FILE_PATH).arg(rootPath), QSettings::IniFormat,0);
    config.beginGroup("logging");
    config.setValue("path", options[cfgLogPath].toString());
    config.setValue("level", options[cfgLogLevel].toString());
    config.endGroup();
}

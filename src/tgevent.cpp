#include "tnetportal.h"


static QMap<QString, QString> EvClassMap;
static QMap<QString, QString> EvTopicMap;





/**
 * @brief processTempAlarmEvent
 * @param rawData
 * @return
 * @details Process comma delimited string of raw data into sorted data
 *          Global alarm, pos of sensor, name of sensor, alarm of sensor, a1 of sensor, a2 of sensor, alarm 1/2, rising/falling
 */
/*static QVariant processTempAlarmEvent(QString &rawData, bool *result)
{
    QStringList tokens = rawData.split(",");

    if (tokens.count() != 8) {
        qDebug() << "ProcessTempAlarm: Token size incorrect, got " << tokens.count() << " expecting 8";
        *result = false;
        return QVariant();
    }

    *result = true;
    return QVariant(tokens);
}*/

/**
 * @brief TnetEvent::TnetEvent
 */
TnetEvent::TnetEvent(QObject * parent, const QString &rpath):
    QThread(parent),
    rootPath(rpath)
{
    EvClassMap.insert(EVCLASS_LIVE, "Live");
    EvClassMap.insert(EVCLASS_TEMP, "Temperature");
    EvClassMap.insert(EVCLASS_SYSTEM, "System");
    EvClassMap.insert(EVCLASS_HAMACHI, "Hamachi");
    EvClassMap.insert(EVCLASS_NETWORK, "Network");
    EvClassMap.insert(EVCLASS_EMAIL, "Email");
    EvClassMap.insert(EVCLASS_SMS, "Sms");
    EvClassMap.insert(EVCLASS_GATEWAY, "Gateway");
    EvClassMap.insert(EVCLASS_AUDVIS, "AudioVisual");

    EvTopicMap.insert(EVTOPIC_TEMP_NO_CONFIG, "No configuration");
    EvTopicMap.insert(EVTOPIC_TEMP_NO_CONFIG, "No configuration");
    EvTopicMap.insert(EVTOPIC_TEMP_CTRL_FAULT, "1w controller fault");
    EvTopicMap.insert(EVTOPIC_TEMP_ALRM_A1, "Alarm 1");
    EvTopicMap.insert(EVTOPIC_TEMP_ALRM_A2, "Alarm 2");
    EvTopicMap.insert(EVTOPIC_TEMP_STOP_SESH, "Stop session");
    EvTopicMap.insert(EVTOPIC_TEMP_NEW_SESH, "New session");
    EvTopicMap.insert(EVTOPIC_TEMP_RESTART_SESH, "Restart session");
    EvTopicMap.insert(EVTOPIC_TEMP_RESUME_SESH, "Resume session");
    EvTopicMap.insert(EVTOPIC_TEMP_NO_SENSORS, "No sensors");
    EvTopicMap.insert(EVTOPIC_TEMP_NEW_DATA, "New sample");
    EvTopicMap.insert(EVTOPIC_SYS_NO_CONFIG, "No configuration");
    EvTopicMap.insert(EVTOPIC_SYS_SHUTDOWN, "Shutdown");
    EvTopicMap.insert(EVTOPIC_SYS_RESTART, "Restart");
    EvTopicMap.insert(EVTOPIC_SYS_POWERON, "Power on");
    EvTopicMap.insert(EVTOPIC_SYS_UPDATING, "Software update");
    EvTopicMap.insert(EVTOPIC_SYS_DISKFULL, "Disk full");
    EvTopicMap.insert(EVTOPIC_SYS_POWERCHANGE, "Power state change");
    EvTopicMap.insert(EVTOPIC_SYS_BATTERYLOW, "Battery low");
    EvTopicMap.insert(EVTOPIC_HAM_NO_CONFIG, "No configuration");
    EvTopicMap.insert(EVTOPIC_HAM_JOINED, "Join network");
    EvTopicMap.insert(EVTOPIC_HAM_LEAVE, "Leave network");
    EvTopicMap.insert(EVTOPIC_HAM_OFFLINE, "Offline");
    EvTopicMap.insert(EVTOPIC_HAM_ONLINE, "Online");
    EvTopicMap.insert(EVTOPIC_GWY_NO_CONFIG, "No configuration");
    EvTopicMap.insert(EVTOPIC_GWY_PROVISIONED, "Not provisioned");
    EvTopicMap.insert(EVTOPIC_GWY_FACTORY_RESET, "Factory reset");
    EvTopicMap.insert(EVTOPIC_GWY_NEW_ADMIN_PSK, "Password changed");
    EvTopicMap.insert(EVTOPIC_GWY_PSK_RESET, "Password reset");
    EvTopicMap.insert(EVTOPIC_AUV_STATE_A2, "Password reset");
    EvTopicMap.insert(EVTOPIC_EML_NO_CONFIG, "No configuration");
    EvTopicMap.insert(EVTOPIC_EML_SMTP_FAIL, "Smtp failed");
    EvTopicMap.insert(EVTOPIC_EML_SEND_FAIL, "Send fail");
    EvTopicMap.insert(EVTOPIC_SMS_NO_CONFIG, "No configuration");
    EvTopicMap.insert(EVTOPIC_SMS_SEND_FAIL, "Send failed");
    EvTopicMap.insert(EVTOPIC_NET_NO_CONFIG, "No configuration");
    EvTopicMap.insert(EVTOPIC_NET_IFACECHANGE, "Interface changed");
    EvTopicMap.insert(EVTOPIC_NET_NOINET, "No Internet");

    TG_DEBUG("TnetEvent constructor");
}

/**
 * @brief TnetEvent::~TnetEvent
 */
TnetEvent::~TnetEvent()
{
    if (isRunning()) {
        quit();
        wait();
    }
    TG_DEBUG("TnetEvent destructor");
}

/**
 * @brief TnetEvent::loadAll
 */
void TnetEvent::loadEvents()
{
    start();
}

/**
 * @brief TnetEvent::getEvents
 * @return
 */
const QList<TnetEvent_t> &TnetEvent::getEvents()
{
    return allEvents;
}

/**
 * @brief TnetEvent::filterClass
 */
/*const QList<TnetEvent_t> &TnetEvent::filterClass(void)
{
    return events;
}*/


/**
* @brief TnetEvent::filterTopic
*/
/*const QList<TnetEvent_t> &TnetEvent::filterTopic(void)
{
    return events;
}*/

/**
 * @brief TnetEvent::filterPriority
 */
/*const QList<TnetEvent_t> &TnetEvent::filterPriority(void)
{
    return events;
}*/

/**
 * @brief TnetEvent::search
 * @param search
 */
/*const QList<TnetEvent_t> &TnetEvent::search(QString search)
{
    return events;
}*/

/**
 * @brief TnetEvent::run
 * @details THis method is run in the thread
 */
void TnetEvent::run()
{
    TG_DEBUG("Thread id = " << currentThreadId());
    allEvents.clear();

    QDir eventDir(TNET_DEVICE_EVENT_DIR);

    // directory exists ?
    if (!eventDir.exists()) {
        TG_WARNING("Event dir " << eventDir.dirName() << " does not exist");
        emit finished(false);
        return;
    }

    // Estimate number of samples for the session by only counting the log files
    // Sort by oldest at top of list and tdata.log at bottom of list
    QStringList filters;
    filters << "events.log*";
    eventDir.setNameFilters(filters);
    eventDir.setSorting(QDir::Time | QDir::Reversed);
    QStringList logFilesEntry = eventDir.entryList();
    int fileCount = logFilesEntry.count();
    TG_DEBUG("Found " << fileCount << " event log files");
    TG_DEBUG("Event files list sorted by reverse date");
    for (int i=0; i<fileCount; i++)
       TG_DEBUG("   " << logFilesEntry.at(i));

    QFile dataFile;

    try {
        // load files
        for(int i=0; i<fileCount; i++) {

            TG_DEBUG( "Loading file " << logFilesEntry.at(i));
            dataFile.setFileName(QString(TNET_DEVICE_EVENT_FILE).arg(logFilesEntry.at(i)));

            if (!dataFile.exists()) {
                TG_WARNING("Event file does not exist");
                continue;
            }


            // open file
            if (!dataFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
                TG_WARNING("Unable to open " << dataFile.fileName());
                continue;
            }

            qulonglong dtSample;

            // process file
            // lines have following format
            // UTCtime:EventClass:EventTopic:EventPriority:EventData
            while(!dataFile.atEnd()) {
                TnetEvent_t ev;

                QString line = QString(dataFile.readLine());

                // ensure there is a comma delimiter in the line
                if (!line.contains(":")){
                    TG_DEBUG("Line has no : delimeter");
                    continue;
                }

                QStringList tokens = line.split(':');

                // verify should be 5 tokens
                if (tokens.count() != 5) {
                    TG_DEBUG("Tokens line count incorrect got " << tokens.count() << " expecting 5");
                    continue;
                }

                // datetime as utc number
                // rough validation should be between 2017 and 2050
                dtSample = tokens.at(0).toULongLong();
                if (dtSample < (qulonglong)1483142400 || dtSample > (qulonglong)2556057600)
                    continue;

                ev.dtUtc = dtSample;

                // class
                if (!EvClassMap.contains(tokens.at(1))){
                    TG_DEBUG("Unknown event class type " << tokens.at(1));
                    continue;
                }

                ev.evClass = EvClassMap[tokens[1]];

                // topic
                if (!EvTopicMap.contains(tokens.at(2))){
                    TG_DEBUG("Unknown event topic type " << tokens.at(2));
                    continue;
                }

                ev.evTopic = EvTopicMap[tokens[2]];

                // priority


                if (tokens[4].contains(",")) {
                    QStringList data = tokens[4].split(",");
                    ev.evData = QVariant(data);
                } else if (!tokens[4].isEmpty())
                    ev.evData = QVariant(tokens[4]);
                else
                    ev.evData = QVariant();


                // data
               /* bool ok = false;
                ev.evData = processTempAlarmEvent(tokens[4], &ok);
                if (!ok)
                    continue;*/


                allEvents.prepend(ev);
            }

            dataFile.close();
        }

    } catch (TgException &e) {
        TG_WARNING("Exception occured e=" << e.what());
        dataFile.close();
        emit finished(false);
        return;
    }

    emit finished(true);
}





#include "tnetportal.h"


/**
 * @brief TgGateway::TgGateway
 * @param parent
 * @param logger
 */
TgGateway::TgGateway(QObject *parent, const QString rpath):
    QObject(parent),
    rootPath(rpath),
    cacheSize(600),
    wifiScanInProgress(false),
    requestAllConfigAfterSessionChange(false),
    connected(false)
{
    netinfo.ham = false;
    muteStatus.muted = false;
    muteStatus.muteTimeRemaining = 0;
    muteStatus.muteActionsTaken = 0;



    zip = 0;
    workerThread = 0;

    TG_DEBUG("Device path = " << rootPath.toLatin1().data());
    tgstream = new TgStream(this);
    QObject::connect(tgstream, SIGNAL(streamConnected()), this, SLOT(streamConnected()));
    QObject::connect(tgstream, SIGNAL(streamDisconnected()), this, SLOT(streamDisconnected()));
    QObject::connect(tgstream, SIGNAL(sendEvent(QString,QString,QString,TgEvStreamPriority)), this, SLOT(on_event(QString,QString,QString,TgEvStreamPriority)));

    manager = new QNetworkAccessManager(this);
    QObject::connect(manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(managerReply(QNetworkReply*)));
    QObject::connect(&networkTimer, SIGNAL(timeout()), this, SLOT(networkTimerTimeout()));

    TG_DEBUG("Gateway constructed");
}

/**
 * @brief TgGateway::~TgGateway
 */
TgGateway::~TgGateway()
{
    networkTimer.stop();
    delete manager;
    delete tgstream;
    try {
        if (zip)
            delete zip;
        if (workerThread)
            delete workerThread;
    } catch (TgException &e){
        TG_DEBUG("Gateway constructing, exception = " << e.what());
    }
    TG_DEBUG("Gateway destructed");
}

bool TgGateway::isOnline()
{
    return connected;
}

/**
 * @brief TgGateway::requestAllConfig
 * @details Get all config from server.
 */
void TgGateway::requestAllConfig(void)
{
    ctxRequest = getAllCfg;
    QUrl url = QUrl(QString("http://%1:%2%3").arg(
                        ipAddress).arg(
                        TG_CONFIG_DEFAULT_MANAGER_PORT).arg(
                        HTTP_REQUEST_ALL_CONFIG));
    TG_DEBUG("Request all config url = " << url.toString());
    manager->get(QNetworkRequest(url));

    networkTimer.start(TNET_TIMEOUT);
}

/**
 * @brief TgGateway::connectStream
 */
void TgGateway::connectStream()
{
    // connect stream
    tgstream->connect(ipAddress,TG_CONFIG_DEFAULT_STREAM_PORT);
}

void TgGateway::disconnectStream()
{
    tgstream->disconnect();
}

/**
 * @brief TgGateway::setCacheLimit
 * @param size
 */
void TgGateway::setCacheLimit(const int size)
{
     this->cacheSize = size;
     TG_INFO("Set cache size to " << size);
}

/**
 * @brief TgGateway::getCacheSize
 * @return
 */
int TgGateway::getCacheSize() const
{
    return streamPoints.count();
}

/**
 * @brief TgGateway::enableLocalHostMode
 */
void TgGateway::enableLocalHostMode(void)
{
    ipAddress = QString("127.0.0.1");
}

/**
 * @brief TgGateway::setIpAddress
 * @param ipaddr
 */
void TgGateway::setIpAddress(QString ipaddr)
{
    ipAddress = ipaddr;
    TG_DEBUG("Setting Ip address to " << ipAddress.toLatin1().data());
}

bool TgGateway::sessionHasData(const QString& sessionNumber, bool checkConfig)
{
    if (checkConfig){
        QFile tjson(QString(TNET_DEVICE_SESSION_DATA_FILE).arg(rootPath,sessionNumber,"temperature.json"));
        if (!tjson.exists()){
            TG_DEBUG("Session " << sessionNumber << " has no temperature.json");
            return false;
        }
    }

    QDir dataDir(QString(TNET_DEVICE_SESSION_DIR).arg(rootPath,sessionNumber));
    QStringList filters;
    filters << "tdata*";
    dataDir.setNameFilters(filters);
    QStringList dataFiles = dataDir.entryList();

    TG_DEBUG("Session " << sessionNumber << " has " << dataFiles.count() << " data files");
    if (dataFiles.count() == 0)
        return false;
    else
        return true;
}

QString TgGateway::getSessionName(const QString &sessionNumber)
{
    TgJson json;
    bool result;

    // gateway json
    QVariantMap tgTemp = json.parseFile(
                QString(TNET_DEVICE_SESSION_DATA_FILE).arg(rootPath, sessionNumber,"temperature.json"),
                result).toMap();

    if (!result)
        return QString();

    try {

        QVariantMap sessionMap = tgTemp["Session"].toMap();
        return sessionMap["Alias"].toString();

    } catch (TgException &e) {
        TG_ERROR("Exception e=" << e.what());
        return QString();
    }

}

/**
 * @brief TgGateway::requestSessionRestart
 * @details Start a new session but keep all previous session details
 */
void TgGateway::requestSessionRestart(void)
{
    ctxRequest = postSessionRestart;
    QUrl url = QUrl(QString("http://%1:%2%3").arg(ipAddress).arg(
                                            TG_CONFIG_DEFAULT_MANAGER_PORT).arg(
                                            HTTP_REQUEST_SESSION_RESTART));
    TG_DEBUG("Post request session restart url = " << url.toString());

    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    manager->post(QNetworkRequest(url), QByteArray());
    networkTimer.start(TNET_TIMEOUT);
}

/**
 * @brief TgGateway::requestSessionResume
 * @param newSession
 * @param sensors
 * @details Resume session with some changed settings
 */
void TgGateway::requestSessionResume(const Session_t *newSession, const QVector<Sensor_t> *newSensors)
{
    ctxRequest = postSessionResume;
    QUrl url = QUrl(QString("http://%1:%2%3").arg(ipAddress).arg(
                                            TG_CONFIG_DEFAULT_MANAGER_PORT).arg(
                                            HTTP_REQUEST_SESSION_RESUME));

    // save for reply
    sessionRequest = newSession;
    sensorsRequest = newSensors;

    TG_DEBUG("Post session resume url = " << url.toString());

    // set content as json for network request
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    // create json data {"Ssid": "" , "Psk": ""}
    QVariantMap sessionMap;
    sessionMap.insert("Number", sessionRequest->sessionnumber);
    sessionMap.insert("Alias", sessionRequest->sessionname);
    sessionMap.insert("AlarmType", sessionRequest->alarmtype);
    sessionMap.insert("TriggerRate", sessionRequest->triggerrate);
    sessionMap.insert("TotalSensors", sessionRequest->totalSensors);

    QVariantMap sensorsMap;
    for (int i=0; i<sessionRequest->totalSensors; i++) {
        QVariantMap sensorMap;
        sensorMap.insert("Pos", sensorsRequest->at(i).pos);
        sensorMap.insert("Serial", sensorsRequest->at(i).serial);
        sensorMap.insert("Alias", sensorsRequest->at(i).alias);
        sensorMap.insert("A1", sensorsRequest->at(i).a1);
        sensorMap.insert("A2", sensorsRequest->at(i).a2);
        sensorMap.insert("A1trig", sensorsRequest->at(i).a1trig);
        sensorMap.insert("A2trig", sensorsRequest->at(i).a2trig);
        sensorMap.insert("Diffmode", sensorsRequest->at(i).diffmode);
        sensorsMap.insert(QString("%1").arg(i+1), sensorMap);
    }

    QVariantMap resumeMap;
    resumeMap.insert("Session", sessionMap);
    resumeMap.insert("Sensors", sensorsMap);

    TgJson json;
    bool result;
    QByteArray jsondata = json.serialise(resumeMap, &result);

    TG_DEBUG("Post session resume json data " << jsondata);

    manager->post(request, jsondata);
    TG_DEBUG("Post session resume");
    networkTimer.start(TNET_TIMEOUT);
}

/**
 * @brief TgGateway::requestSessionNew
 * @param newSession
 * @param sensors
 */
void TgGateway::requestSessionNew(const Session_t *newSession, const QVector<Sensor_t> *newSensors)
{
    ctxRequest = postSessionNew;

    QUrl url = QUrl(QString("http://%1:%2%3").arg(ipAddress).arg(
                                            TG_CONFIG_DEFAULT_MANAGER_PORT).arg(
                                            HTTP_REQUEST_SESSION_NEW));

    // save for reply
    sessionRequest = newSession;
    sensorsRequest = newSensors;

    TG_DEBUG("Post session new url = " << url.toString());

    // set content as json for network request
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    // create json data {"Number": "" , "Alias": "" ... }
    QVariantMap sessionMap;
    sessionMap.insert("Number", sessionRequest->sessionnumber);
    sessionMap.insert("Alias", sessionRequest->sessionname);
    sessionMap.insert("AlarmType", sessionRequest->alarmtype);
    sessionMap.insert("TriggerRate", sessionRequest->triggerrate);
    sessionMap.insert("TotalSensors", sessionRequest->totalSensors);

    QVariantMap sensorsMap;
    for (int i=0; i<sessionRequest->totalSensors; i++) {
        QVariantMap sensorMap;
        sensorMap.insert("Pos", sensorsRequest->at(i).pos);
        sensorMap.insert("Alias", sensorsRequest->at(i).alias);
        sensorMap.insert("Serial", sensorsRequest->at(i).serial);
        sensorMap.insert("A1", sensorsRequest->at(i).a1);
        sensorMap.insert("A2", sensorsRequest->at(i).a2);
        sensorMap.insert("Diffmode", sensorsRequest->at(i).diffmode);
        sensorsMap.insert(QString("%1").arg(i+1), sensorMap);
    }

    QVariantMap newMap;
    newMap.insert("Session", sessionMap);
    newMap.insert("Sensors", sensorsMap);

    TgJson json;
    bool result;
    QByteArray jsondata = json.serialise(newMap, &result);

    TG_DEBUG("Post session new json data " << jsondata);

    manager->post(request, jsondata);
    TG_DEBUG("Post session new");

    networkTimer.start(TNET_TIMEOUT);
}

/**
 * @brief TgGateway::requestRecords
 * @param session
 * @param filename
 */
void TgGateway::requestRecords(const QString session, const QString filename)
{
    ctxRequest = getTempRecords;
    QUrl url = QUrl(QString("http://%1:%2%3?session=%4&file=%5").arg(ipAddress).arg(
                        TG_CONFIG_DEFAULT_MANAGER_PORT).arg(
                        HTTP_REQUEST_TEMP_RECORDS).arg(
                        session.toLatin1().data()).arg(
                        filename.toLatin1().data()));
    TG_DEBUG("Request temperature records url = " << url.toString());
    manager->get(QNetworkRequest(url));
    networkTimer.start(TNET_TIMEOUT);
}

void TgGateway::requestUpdateUsers(const QVector<UserInfo_t> *users)
{
    QUrl url = QUrl(QString("http://%1:%2%3").arg(ipAddress).arg(
                                            TG_CONFIG_DEFAULT_MANAGER_PORT).arg(
                                            HTTP_REQUEST_USERS));
    ctxRequest = postUsers;
    // save for reply
    usersCopy = users;

    TG_DEBUG("Post users url = " << url.toString());

    // set content as json for network request
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    // create json data {"Number": "" , "Alias": "" ... }

    QVariantMap notificationsMap;
    QVariantMap usersMap;
    notificationsMap.insert("Email", notifications.email);
    notificationsMap.insert("Sms", notifications.sms);

    for (int i=0; i<usersCopy->count(); ++i){
        QVariantMap userMap;
        QVariantMap alertsMap;
        alertsMap.insert("LowBattery", usersCopy->at(i).alerts.lowBattery);
        alertsMap.insert("SessionNew", usersCopy->at(i).alerts.newSession);
        alertsMap.insert("SessionResume", usersCopy->at(i).alerts.resumeSession);
        alertsMap.insert("A2Alarms", usersCopy->at(i).alerts.a2Alarms);
        alertsMap.insert("A1Alarms", usersCopy->at(i).alerts.a1Alarms);
        alertsMap.insert("PowerOff", usersCopy->at(i).alerts.powerOff);
        alertsMap.insert("SessionRestart", usersCopy->at(i).alerts.restartSession);
        alertsMap.insert("SessionStop", usersCopy->at(i).alerts.stopSession);
        alertsMap.insert("PowerChange", usersCopy->at(i).alerts.powerChange);
        alertsMap.insert("IfaceChange", usersCopy->at(i).alerts.netChange);
        alertsMap.insert("PowerOn",usersCopy->at(i).alerts.powerOn);

        userMap.insert("Email", usersCopy->at(i).email);
        userMap.insert("Mobile", usersCopy->at(i).phone);
        userMap.insert("Alerts", alertsMap);

        usersMap.insert(usersCopy->at(i).name, userMap);
    }
    notificationsMap.insert("Users", usersMap);

    TgJson json;
    bool result;
    QByteArray jsondata = json.serialise(notificationsMap, &result);

    TG_DEBUG("Post users json data " << jsondata);

    manager->post(request, jsondata);
    TG_DEBUG("Post user");

    networkTimer.start(TNET_TIMEOUT);
}

void TgGateway::requestUpdateCredentials(const GatewayId_t *credentials)
{
    QUrl url = QUrl(QString("http://%1:%2%3").arg(ipAddress).arg(
                                            TG_CONFIG_DEFAULT_MANAGER_PORT).arg(
                                            HTTP_REQUEST_CREDENTIALS));
    ctxRequest = postId;
    // save for reply
    id = credentials;

    TG_DEBUG("Post id url = " << url.toString());

    // set content as json for network request
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    // create json data {"Number": "" , "Alias": "" ... }

    QVariantMap credMap;
    QVariantMap idmap;
    idmap.insert("Alias", id->name);
    idmap.insert("Serial", gwyId.serial);
    idmap.insert("D1", id->description);
    idmap.insert("D2","");
    idmap.insert("D3","");
    idmap.insert("D4","");
    idmap.insert("D5","");
    QVariantMap infoMap;
    infoMap.insert("Owner",gwyId.owner);
    infoMap.insert("Contact",gwyId.contact);
    infoMap.insert("InstallDate",gwyId.installdate);
    QVariantMap revMap;
    revMap.insert("Lcd",gwyId.lcdversion);
    revMap.insert("Server",gwyId.serverversion);
    revMap.insert("Hw",gwyId.hwversion);

    credMap.insert("Version",revMap);
    credMap.insert("Id", idmap);
    credMap.insert("Info",infoMap);



    TgJson json;
    bool result;
    QByteArray jsondata = json.serialise(credMap, &result);

    TG_DEBUG("Post id json data " << jsondata);

    manager->post(request, jsondata);
    TG_DEBUG("Post id");

    networkTimer.start(TNET_TIMEOUT);
}

void TgGateway::requestAVConfig(const InputOutput_t *av)
{
    ctxRequest = postAVConfig;

    audioVisual = av;

    QUrl url = QUrl(QString("http://%1:%2%3").arg(ipAddress).arg(
                                            TG_CONFIG_DEFAULT_MANAGER_PORT).arg(
                                            HTTP_REQUEST_AV_CONFIG));


    TG_DEBUG("Post audio visual config url = " << url.toString());

    QVariantMap avMap;
    avMap.insert("Audio", av->audioalert);
    avMap.insert("Visual", av->visualalert);
    avMap.insert("AudioMute",av->mutecount);
    avMap.insert("AudioMuteTimeout", av->mutetime);
    avMap.insert("Buzzer", true);


    TgJson json;
    bool result;
    QByteArray jsondata = json.serialise(avMap, &result);

    TG_DEBUG("Post audio visual config json data " << jsondata);
    // set content as json for network request
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    manager->post(request, jsondata);

    networkTimer.start(TNET_TIMEOUT);
}

void TgGateway::requestMuteAlarm()
{
    ctxRequest = postMuteAlarm;

    QUrl url = QUrl(QString("http://%1:%2%3").arg(ipAddress).arg(
                                            TG_CONFIG_DEFAULT_MANAGER_PORT).arg(
                                            HTTP_REQUEST_MUTE_ALARM));


    TG_DEBUG("Post mute url = " << url.toString());

    // set content as json for network request
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    manager->post(request, QByteArray());

    networkTimer.start(TNET_TIMEOUT);
}

void TgGateway::requestPowerOff(bool reboot)
{
    QUrl url;
    ctxRequest = postPowerOff;

    if (reboot)
         url = QUrl(QString("http://%1:%2%3?reboot=true").arg(ipAddress).arg(
                                            TG_CONFIG_DEFAULT_MANAGER_PORT).arg(
                                            HTTP_REQUEST_POWER_OFF));
    else
         url = QUrl(QString("http://%1:%2%3?reboot=false").arg(ipAddress).arg(
                                            TG_CONFIG_DEFAULT_MANAGER_PORT).arg(
                                            HTTP_REQUEST_POWER_OFF));

    TG_DEBUG("Post power off url = " << url.toString());

    // set content as json for network request
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    manager->post(request, QByteArray());

    networkTimer.start(TNET_TIMEOUT);
}

void TgGateway::requestEmailImages(const QStringList &images, const QStringList &recipients)
{
    QUrl url;
    ctxRequest = postEmailImages;

    url = QUrl(QString("http://%1:%2%3").arg(ipAddress).arg(
                                            TG_CONFIG_DEFAULT_MANAGER_PORT).arg(
                                            HTTP_REQUEST_EMAIL_IMAGES));


    TG_DEBUG("Email images url = " << url.toString());


    // set content as json for network request
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    QVariantMap content;
    content.insert("AddressList", QVariant(recipients));
    content.insert("ImagePaths", QVariant(images));

    TgJson json;
    bool result;
    QByteArray jsondata = json.serialise(content, &result);

    TG_DEBUG("Post email images content " << jsondata);

    // set content as json for network request
    manager->post(request, jsondata);

    networkTimer.start(TNET_TIMEOUT);
}

void TgGateway::requestUpdateKey(const QString &password)
{
    ctxRequest = postKey;
    hashedKey = hashPassword(password);
    QUrl url = QUrl(QString("http://%1:%2%3?key=%4").arg(
                        ipAddress).arg(
                        TG_CONFIG_DEFAULT_MANAGER_PORT).arg(
                        HTTP_REQUEST_KEY).arg(
                        QString(hashedKey)));
    TG_DEBUG("Request update key url = " << url.toString());
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    manager->post(request, QByteArray());
    networkTimer.start(TNET_TIMEOUT);
}





/**
 * @brief TgGateway::requestWifiScan
 * @details Request Wifi scan
 */
void TgGateway::requestWifiScan(void)
{
    ctxRequest = getWifiScan;
    wifiScanInProgress = true;
    networks.clear();

    QUrl url = QUrl(QString("http://%1:%2%3").arg(ipAddress).arg(
                        TG_CONFIG_DEFAULT_MANAGER_PORT).arg(
                        HTTP_REQUEST_WIFI_SCAN));
    TG_DEBUG("Request wifi scan url = " << url.toString());
    manager->get(QNetworkRequest(url));
    TG_DEBUG("TgGateway: Request wifi scan");

    networkTimer.start(TNET_TIMEOUT);
}

/**
 * @brief TgGateway::connectWifiNetwork
 * @param ssid
 * @param password
 */
void TgGateway::connectWifiNetwork(QString ssid, QString password)
{
    ctxRequest = postWifiConnect;
    QUrl url = QUrl(QString("http://%1:%2%3").arg(ipAddress).arg(
                        TG_CONFIG_DEFAULT_MANAGER_PORT).arg(
                        HTTP_REQUEST_WIFI_CONNECT));
    TG_DEBUG("Request wifi connect url = " << url.toString());

    // set content as json for network request
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    // find the wifi that matches the ssid and set it's pending action connecting
    for (int i=0; i< networks.count(); i++){
        if (networks.at(i).ssid == ssid){
            WifiNetwork_t * network = &networks[i];
            network->pendingAction = paConnecting;
        }
    }

    // create json data {"Ssid": "" , "Psk": ""}
    QVariantMap wifiDetails;
    wifiDetails.insert("Ssid", ssid);
    wifiDetails.insert("Psk", password);
    TgJson json;
    bool result;
    QByteArray jsondata = json.serialise(wifiDetails, &result);

    TG_DEBUG("Json data " << jsondata);

    manager->post(request, jsondata);
    TG_DEBUG("Request wifi connect");

    networkTimer.start(TNET_TIMEOUT);
}

/**
 * @brief TgGateway::disconnectWifiNetwork
 * @param ssid
 */
void TgGateway::disconnectWifiNetwork(QString ssid)
{
    ctxRequest = postWifiDisconnect;
    QUrl url = QUrl(QString("http://%1:%2%3").arg(ipAddress).arg(
                        TG_CONFIG_DEFAULT_MANAGER_PORT).arg(
                        HTTP_REQUEST_WIFI_DISCONNECT));
    TG_DEBUG("Request wifi disconnect url = " << url.toString());

    // set content as json for network request
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    // create json data {"Ssid": "" , "Psk": ""}

    // find the wifi that matches the ssid and set it's pending action disconnecting
    for (int i=0; i< networks.count(); i++){
        if (networks.at(i).ssid == ssid){
            WifiNetwork_t * network = &networks[i];
            network->pendingAction = paDisconnecting;
        }
    }

    QVariantMap wifiDetails;
    wifiDetails.insert("Ssid", ssid);
    TgJson json;
    bool result;
    QByteArray jsondata = json.serialise(wifiDetails, &result);

    TG_DEBUG("Json data " << jsondata);

    manager->post(request, jsondata);
    TG_DEBUG("Request wifi disconnect");

    networkTimer.start(TNET_TIMEOUT);
}





/*************************************
 *  Stream API
 *************************************/

/**
 * @brief TgGateway::streamConnected
 */
void TgGateway::streamConnected()
{
#ifdef TNET_DESKTOP
    TG_WARNING("Gateway connected");
    connected = true;
    emit connectionStateChange(true);
#endif
}

/**
 * @brief TgGateway::streamDisconnected
 */
void TgGateway::streamDisconnected()
{
#ifdef TNET_DESKTOP
    TG_WARNING("Gateway disconnected");
    connected = false;
    emit connectionStateChange(false);
#endif
}



/**
 * @brief TgGateway::networkTimerTimeout
 * @details Network timeout handler incase get/post url times out
 */
void TgGateway::networkTimerTimeout()
{
    networkTimer.stop();
    requestResult(false,"Request timeout");

    switch(ctxRequest) {
        case getAllCfg:
        case getWifiScan:
        case getTempRecords:
        case postWifiConnect:

        case postSessionRestart:
        case postPowerOff:
        case postMuteAlarm:
        case postEmailImages:
            break;

        case postSessionResume:
        case postSessionNew:
            sessionRequest = 0;
            sensorsRequest = 0;
            break;

        case postUsers:
            usersCopy = 0;
            break;

        case postId:
            id = 0;
            break;

        case postAVConfig:
            audioVisual = 0;
            break;

        case postWifiDisconnect:
            // set pending action to none
            for (int i=0; i< networks.count(); i++){
                if (networks.at(i).pendingAction != paNone){
                    WifiNetwork_t * network = &networks[i];
                    network->pendingAction = paNone;
                }
            }
            break;

        default:
            break;
    }
}

/**
 * @brief TgGateway::managerReply
 * @param reply
 */
void TgGateway::managerReply(QNetworkReply *reply)
{
    TG_DEBUG("Manager reply");

    try {

        bool ok;
        TgJson parser;

        // tempRecords GET request contains octet stream data not json
        QNetworkRequest request = reply->request();
        if ( request.url().toString().contains(HTTP_REQUEST_TEMP_RECORDS) ){
            networkTimer.stop();
            netReplyTempRecords(reply);
            reply->deleteLater();
            return;
        }

        QVariantMap responseMap = parser.parse(reply->readAll(), ok).toMap();

        if (!ok) {
            TG_WARNING("Unable to parse json content");

        // response must contain all 3 keys
        } else if (!responseMap.contains("Request") ||
            !responseMap.contains("Result") ||
            !responseMap.contains("Repr")) {

            TG_WARNING("Response does not contain some keys");

        } else {

            TG_DEBUG("Http response request = " << responseMap["Request"].toString());
            TG_DEBUG("Http response result = " << responseMap["Result"].toInt());
            TG_DEBUG("Http response repr = " << responseMap["Repr"].toString());

            // check original request matches request in response
            if (!request.url().toString().contains(responseMap["Request"].toString())) {
                TG_WARNING("Http response does not match original request");

            } else {

                // stop timeout timer
                networkTimer.stop();

                // map for http response handlers
                if ( responseMap["Request"].toString() == HTTP_REQUEST_ALL_CONFIG )
                    netReplyAllConfig(responseMap);
                else if ( responseMap["Request"].toString() == HTTP_REQUEST_WIFI_SCAN )
                    netReplyWifiScan(responseMap);
                else if ( responseMap["Request"].toString() == HTTP_REQUEST_WIFI_CONNECT )
                    netReplyWifiConnect(responseMap);
                else if ( responseMap["Request"].toString() == HTTP_REQUEST_WIFI_DISCONNECT )
                    netReplyWifiDisconnect(responseMap);
                else if ( responseMap["Request"].toString() == HTTP_REQUEST_SESSION_RESUME )
                    netReplySessionResume(responseMap);
                else if ( responseMap["Request"].toString() == HTTP_REQUEST_SESSION_RESTART )
                    netReplySessionRestart(responseMap);
                else if ( responseMap["Request"].toString() == HTTP_REQUEST_SESSION_NEW )
                    netReplySessionNew(responseMap);
                else if ( responseMap["Request"].toString() == HTTP_REQUEST_USERS )
                    netReplyUsers(responseMap);
                else if ( responseMap["Request"].toString() == HTTP_REQUEST_CREDENTIALS )
                    netReplyCredentials(responseMap);
                else if ( responseMap["Request"].toString() == HTTP_REQUEST_POWER_OFF )
                    netReplyPowerOff(responseMap);
                else if ( responseMap["Request"].toString() == HTTP_REQUEST_MUTE_ALARM )
                    netReplyMuteAlarm(responseMap);
                else if ( responseMap["Request"].toString() == HTTP_REQUEST_EMAIL_IMAGES )
                    netReplyEmailImages(responseMap);
                else if (responseMap["Request"].toString() == HTTP_REQUEST_AV_CONFIG)
                    netReplyAVConfig(responseMap);
                else if (responseMap["Request"].toString() == HTTP_REQUEST_KEY)
                    netReplyKeyConfig(responseMap);
            }
        }

    } catch (TgException &e) {     
        TG_ERROR("Exception e=" << e.what());
    }

    // must delete QNetworkReply object as per documentation
    reply->deleteLater();
}





/**
 * @brief TgGateway::netReplyAllConfig
 * @details All config reply
 * @param qm
 *
 */
void TgGateway::netReplyAllConfig(const QVariantMap& qm)
{
    try {
        QVariantMap tgData = qm["Data"].toMap();
        QVariantMap tgGwy = tgData["Creds"].toMap();
        QVariantMap tgInfo = tgGwy["Info"].toMap();
        QVariantMap tgVersion = tgGwy["Version"].toMap();
        QVariantMap tgId = tgGwy["Id"].toMap();
        QVariantMap tgTemp = tgData["Temp"].toMap();
        QVariantMap tgSession = tgTemp["Session"].toMap();
        QVariantMap tgSensors = tgTemp["Sensors"].toMap();
        QVariantMap tgHam = tgData["Ham"].toMap();
        QVariantMap tgNfy = tgData["Users"].toMap();
        QVariantMap tgAv = tgData["Av"].toMap();
        hashedKey = tgData["Key"].toByteArray();



        gwyId.name = tgId["Alias"].toString();
        gwyId.serial = tgId["Serial"].toString();
        gwyId.description = tgId["D1"].toString();

        gwyId.owner = tgInfo["Owner"].toString();
        gwyId.contact = tgInfo["Contact"].toString();
        gwyId.installdate = tgInfo["InstallDate"].toString();
        gwyId.lcdversion = tgVersion["Lcd"].toString();
        gwyId.serverversion = tgVersion["Server"].toString();
        gwyId.hwversion = tgVersion["Hw"].toString();

        session.seshcommit = tgTemp["Commit"].toInt();
        int totalSensors = tgSession["TotalSensors"].toInt();
        session.totalSensors = totalSensors;
        session.sessionname = tgSession["Alias"].toString();
        session.sessionnumber = tgSession["Number"].toString();
        session.alarmtype = (TgAlarmInterpretation)tgSession["AlarmType"].toInt();
        session.triggerrate = tgSession["TriggerRate"].toInt();

        sensors.clear();
        for (int i=0; i<totalSensors; i++) {
           QVariantMap tgSensor = tgSensors[QString("%1").arg(i+1)].toMap();
           Sensor_t sensor;
           sensor.pos = i+1;
           sensor.alias = tgSensor["Alias"].toString();
           sensor.serial = tgSensor["Serial"].toString();
           sensor.a1 = tgSensor["A1"].toInt();
           sensor.a2 = tgSensor["A2"].toInt();
           sensor.diffmode = tgSensor["Diffmode"].toInt();
           sensor.a1trig = tgSensor["A1trig"].toBool();
           sensor.a2trig = tgSensor["A2trig"].toBool();
           sensors.append(sensor);
        }


        netinfo.hamNetId = tgHam["NetId"].toString();
        netinfo.hamClientId = tgHam["ClientId"].toString();
        netinfo.hamPsk = tgHam["NetPass"].toString();
        netinfo.hamIp = tgHam["Ipv4"].toString();

        notifications.email = utilsIntToBool(tgNfy["Email"].toInt());
        notifications.sms = utilsIntToBool(tgNfy["Sms"].toInt());

        // user notifications
        users.clear();
        QVariantMap usersMap = tgNfy["Users"].toMap();
        for (QVariantMap::const_iterator it = usersMap.begin(); it != usersMap.end(); ++it){
            UserInfo_t user;
            QVariantMap userMap = it.value().toMap();
            user.name = it.key();
            user.email = userMap["Email"].toString();
            user.phone = userMap["Mobile"].toString();
            QVariantMap userAlerts = userMap["Alerts"].toMap();
            user.alerts.lowBattery = userAlerts["LowBattery"].toBool();
            user.alerts.netChange = userAlerts["IfaceChange"].toBool();
            user.alerts.powerOn = userAlerts["PowerOn"].toBool();
            user.alerts.powerOff = userAlerts["PowerOff"].toBool();
            user.alerts.powerChange = userAlerts["PowerChange"].toBool();
            user.alerts.stopSession = userAlerts["SessionStop"].toBool();
            user.alerts.restartSession = userAlerts["SessionRestart"].toBool();
            user.alerts.resumeSession = userAlerts["SessionResume"].toBool();
            user.alerts.newSession = userAlerts["SessionNew"].toBool();
            user.alerts.a1Alarms = userAlerts["A1Alarms"].toBool();
            user.alerts.a2Alarms = userAlerts["A2Alarms"].toBool();
            users.append(user);
        }

        inout.visualalert = tgAv["Visual"].toBool();
        inout.audioalert = tgAv["Audio"].toBool();
        inout.mutecount = tgAv["AudioMute"].toUInt();
        inout.mutetime = tgAv["AudioMuteTimeout"].toUInt();
        inout.buzzer = tgAv["Buzzer"].toBool();

        // dump to file
#if defined(TNET_DESKTOP)
        saveConfigGateway();
        saveConfigNetwork();
        saveConfigTemperature();
        saveConfigUsers();
        saveConfigAudioVisual();
        saveConfigKey();
#endif

        printConfig();

        // if getting request after session change don't notify gui
        if (!requestAllConfigAfterSessionChange)
            emit requestResult(true, QString());
        else
            requestAllConfigAfterSessionChange = false;

    } catch (TgException &e) {
        TG_ERROR("Exception e=" << e.what());
        emit requestResult(false, "Parsing error");
    }
}

/**
 * @brief TgGateway::netReplyWifiScan
 * @details Scan result for wifi
 * @param qm
 */
void TgGateway::netReplyWifiScan(const QVariantMap& qm)
{
    networks.clear();

    try {
        QVariantMap datamap = qm["Data"].toMap();
        int total = datamap["Total"].toInt();
        TG_DEBUG(QString("TgGateway: Wifi scan returned %1 networks").arg(total).toLatin1().data());
        if (!total) {
            emit requestResult(true, "No networks found");

        } else {

            QVariantList nets = datamap["Networks"].toList();

            for (int i=0; i<total; i++) {
                QVariantMap netmap = nets.at(i).toMap();
                WifiNetwork_t net;
                net.hidden = false;
                if (netmap["State"].toString() == QString("online"))
                    net.state = wsOnline;
                else if (netmap["State"].toString() == QString("idle"))
                    net.state = wsIdle;
                else if (netmap["State"].toString() == QString("offline"))
                    net.state = wsOffline;
                else
                    net.state = wsIdle;

                net.ssid = netmap["Name"].toString();

                //tglogger->info(QString("    SSID: %1").arg(net.ssid).toLatin1().data());

                QString security = netmap["Security"].toString();
                if (security.contains("Open"))
                    net.security = wsNone;
                else if (security.contains("WPA-PSK"))
                    net.security = wsWpa;
                else if (security.contains("WPA2-PSK"))
                    net.security = wsWpa2;
                else if (security.contains("WEP"))
                    net.security = wsWep;
                else
                    net.security = wsNone;

                net.signal = netmap["Strength"].toInt();

                QVariantMap ipv4 = netmap["Ipv4"].toMap();
                net.ipv4.address = ipv4["Address"].toString();
                net.ipv4.gateway = ipv4["Gateway"].toString();
                net.ipv4.netmask = ipv4["Netmask"].toString();
                QString method = ipv4["Address"].toString();
                if (method == QString("dchp"))
                    net.ipv4.method = ipDhcp;
                else
                    net.ipv4.method = ipStatic;

                net.pendingAction = paNone;
                networks.append(net);
            }

            emit requestResult(true, "Networks found");

        }

    } catch (TgException &e) {
        TG_ERROR("Exception e=" << e.what());
        emit requestResult(false, "No networks found");
    }

    wifiScanInProgress = false;
}

/**
 * @brief TgGateway::netReplyWifiConnect
 * @param qm
 * @details Result = 0(success) or 1 (error)
 *          /TgNet/Wifi/Connect should contain IP address if success and
 *          error repr if failed
 */
void TgGateway::netReplyWifiConnect(const QVariantMap& qm)
{
    try {
        int result = qm["Result"].toInt();
        QString resultStr = qm["Repr"].toString();
        QString ipAddress = qm["Data"].toString();

        TG_DEBUG("Wifi connect result = " << result);

        if (ipAddress.count())
            TG_INFO("Connected on IP address = " << ipAddress);

        if (result == 0) {

            for (int i=0; i< networks.count(); i++){
                if (networks.at(i).pendingAction == paConnecting){
                    WifiNetwork_t * network = &networks[i];
                    network->state = wsOnline;
                    network->pendingAction = paNone;
                }
            }

            emit requestResult(true, resultStr);
        } else {
            TG_DEBUG("Wifi connect error = " << resultStr);
            emit requestResult(false,QString());
        }
    } catch (TgException &e) {
        TG_ERROR("Exception e=" << e.what());
        emit requestResult(false,QString());
    }
}

/**
 * @brief TgGateway::netReplyWifiDisconnect
 * @param qm
 */
void TgGateway::netReplyWifiDisconnect(const QVariantMap &qm)
{
    try {
        int result = qm["Result"].toInt();
        QString resultStr = qm["Repr"].toString();
        QString ipAddress = qm["Data"].toString();

        TG_DEBUG("Wifi disconnect result = " << result);

        if (ipAddress.count())
            TG_INFO("IP address = " << ipAddress);

        if (result == 0) {

            // find ssid with pending action marked as disconnecting

            for (int i=0; i<networks.count(); i++){
                if (networks.at(i).pendingAction == paDisconnecting){
                    WifiNetwork_t * network = &networks[i];
                    network->state = wsOffline;
                    network->pendingAction = paNone;
                }
            }

            emit requestResult(true, resultStr);
        } else {
            TG_DEBUG("Wifi disconnect error = " << resultStr);
            emit requestResult(false,QString());
        }
    } catch (TgException &e) {
        TG_ERROR("Exception e=" << e.what());
        emit requestResult(false,QString());
    }
}


/**
 * @brief TgGateway::netReplySessionRestart
 * @param qm
 * @details Session restart reply {"Result":0, "/TgTmp/Sesh/Restart": "sessionnumber"}
 *          Result = 0 success
 *          Result = 1 fail
 */
void TgGateway::netReplySessionRestart(const QVariantMap& qm)
{
    try {
        int result = qm["Result"].toInt();
        QString restartSession = qm["Data"].toString();
        QString repr = qm["Repr"].toString();
        TG_INFO("Restart session result = " << result << " session = "<< restartSession.toLatin1().data());

        if (result == 0) {

#if defined(TNET_DESKTOP)

            // save temperature.json to session folder
            QFile f(QString(TNET_DEVICE_CONFIG_TEMPERATURE).arg(rootPath));
            if (f.copy(QString(TNET_DEVICE_SESSION_DATA_FILE).arg(rootPath, session.sessionnumber, "temperature.json")))
                TG_DEBUG("Copy temperature.json success");
            else
                TG_WARNING("Copy temperature.json failed");

            session.sessionnumber = restartSession;
            // create new session folder
            QDir d;
            d.mkdir(QString(TNET_DEVICE_SESSION_DIR).arg(rootPath, session.sessionnumber));
            // save the new config to file
            saveConfigTemperature();
#endif

            emit requestResult(true, "Success");
        } else {
            TG_INFO("Restart session failed, reason = " << repr.toLatin1().data());
            emit requestResult(false, "Fail");
        }
    } catch (TgException &e) {
        TG_ERROR("Exception e=" << e.what());
        emit requestResult(false, "Exception");
    }
}

/**
 * @brief TgGateway::netReplySessionResume
 * @param qm
 * @details Session resume reply {"Result":0, "/TgTmp/Sesh/Resume": {} }
 *          Result = 0 success
 *          Result = 1 fail
 */
void TgGateway::netReplySessionResume(const QVariantMap& qm)
{
    try {
        int result = qm["Result"].toInt();
        QString repr = qm["Repr"].toString();

        TG_INFO("Resume session result = " << result << " repr = " << repr.toLatin1().data());

        if (result == 0) {

            session = *sessionRequest;
            sensors = *sensorsRequest;
            sessionRequest = 0;
            sensorsRequest = 0;
#if defined(TNET_DESKTOP)
            saveConfigTemperature();
#endif

            emit requestResult(true, "Success");
        } else {
            TG_INFO("Resume session failed");
            emit requestResult(false, "Fail");
        }
    } catch (TgException &e) {
        TG_ERROR("Exception e=" << e.what());
        emit requestResult(false, "Exception");
    }
}

/**
 * @brief TgGateway::netReplySessionNew
 * @param qm
 * @details Session new reply
 */
void TgGateway::netReplySessionNew(const QVariantMap& qm)
{
    try {
        int result = qm["Result"].toInt();
        QString repr = qm["Repr"].toString();
        QString newSession = qm["Data"].toString();

        TG_INFO("New session result = " << result << " session = "<< newSession.toLatin1().data());

        if (result == 0) {


#if defined(TNET_DESKTOP)
            // save temperature.json to session folder
            QFile f(QString(TNET_DEVICE_CONFIG_TEMPERATURE).arg(rootPath));
            if (f.copy(QString(TNET_DEVICE_SESSION_DATA_FILE).arg(rootPath, session.sessionnumber, "temperature.json")))
                TG_DEBUG("Copy temperature.json success");
            else
                TG_WARNING("Copy temperature.json failed");

            // create new session folder
            QDir d;
            d.mkdir(QString(TNET_DEVICE_SESSION_DIR).arg(rootPath, newSession));
            saveConfigTemperature();
#endif
            // copy saved into local
            session = *sessionRequest;
            sensors = *sensorsRequest;
            session.sessionnumber = newSession;
            sessionRequest = 0;
            sensorsRequest = 0;

            emit requestResult(true, "Success");
        } else {
            TG_INFO("New session failed, reason = " << repr.toLatin1().data());
            emit requestResult(false, "Fail");
        }
    } catch (TgException &e) {
        TG_ERROR("Exception e=" << e.what());
        emit requestResult(false, "Exception");
    }
}

/**
 * @brief TgGateway::netReplyTempRecords
 * @param qm
 */
void TgGateway::netReplyTempRecords(QNetworkReply * reply)
{

    try {

        // get content type and content length
        QString contentType = reply->header(QNetworkRequest::ContentTypeHeader).toString();
        quint32 contentLength = reply->header(QNetworkRequest::ContentLengthHeader).toULongLong();

        TG_DEBUG("Request url = " << reply->url().toString());
        TG_DEBUG("Content type = " << contentType);
        TG_DEBUG("Content length = " << contentLength);

        // error if json returned
        if (contentType == QString("application/json")) {
            bool ok;
            TgJson parser;
            QVariantMap responseMap = parser.parse(reply->readAll(), ok).toMap();

            if (!ok) {
                TG_WARNING("Unable to parse json content");
                emit requestResult(false, "Unable to parse json");
            // response must contain all 3 keys
            } else if (!responseMap.contains("Request") ||
                !responseMap.contains("Result") ||
                !responseMap.contains("Repr")) {

                TG_WARNING("Response does not contain some keys");
                emit requestResult(false, "Response does not contain some keys");
            } else {
                int result = responseMap["Result"].toInt();
                QString repr = responseMap["Repr"].toString();
                TG_WARNING("Result = " << result << " Repr = " << repr.toLatin1().data());
                emit requestResult(false, repr);
            }
            return;
        }

        if (contentType != QString("application/octet-stream")) {
            TG_WARNING("Records request but content type is not octet-stream");
            emit requestResult(false, "Content type not octet-stream");
        }

        if (contentLength == 0){
            TG_WARNING("Records request but content length is 0");
            emit requestResult(false, "Content length 0");
            return;
        }

        if (!reply->url().hasQuery()){
            TG_WARNING("Records request with no query");
            emit requestResult(false, "No query in url");
            return;
        }

#if QT_VERSION >= QT_VERSION_CHECK(5,7,0)
        TG_DEBUG("Query = " << reply->url().query().toLatin1().data());
        QStringList queryoptions = reply->url().query().split('&');
        QStringList sessionoption = QString(queryoptions.at(0)).split('=');

        if (sessionoption.at(0) != QString("session")){
            TG_WARNING("No session key in query");
            emit requestResult(false, "No session key in query");
            return;
        }

        QString session = sessionoption.at(1);

#elif QT_VERSION <= QT_VERSION_CHECK(4,8,7)
        // no QUrl::query() in 4.8

        if (!reply->url().hasQueryItem("session")) {
            TG_WARNING("No session key in query");
            emit requestResult(false, "No session key in query");
            return;
        }

        QString session = reply->url().queryItemValue("session");


#endif

        QString sessionPath = QString(TNET_DEVICE_SESSION_DIR).arg(rootPath,session);
        QDir sessionDir(sessionPath);
        if (!sessionDir.exists()){
            TG_WARNING("Session requested does not exist");
            emit requestResult(false, "Session does not exist");
            return;
        }


        QString tarballfile = QString(TNET_DEVICE_SESSION_DATA_FILE).arg(rootPath,session,QString("tdata.tar.gz"));
        QFile tarball(tarballfile);
        if (!tarball.open(QIODevice::WriteOnly)){
            TG_WARNING("Unable to open file for write");
            emit requestResult(false, "Unable to open file for write");
            return;
        }

        tarball.write(reply->readAll());
        tarball.close();

        TG_DEBUG("Extract " << tarballfile.toLatin1().data() << " to " << sessionPath);
        // now extract and decompress in worker thread
        workerThread = new QThread;
        zip = new TgZip(0, tarballfile, sessionPath);
        zip->moveToThread(workerThread);

        QObject::connect(workerThread, SIGNAL(started()), zip, SLOT(extract()));
        QObject::connect(zip, SIGNAL(extractDone(bool)), this, SLOT(extractFinished(bool)));
        QObject::connect(zip, SIGNAL(finished()), workerThread, SLOT(quit()));
        QObject::connect(zip, SIGNAL(finished()), zip, SLOT(deleteLater()));
        QObject::connect(workerThread, SIGNAL(finished()), workerThread, SLOT(deleteLater()));

        workerThread->start();

    } catch (TgException &e) {
        TG_ERROR("Exception e=" << e.what());
        emit requestResult(false, "Exception");
    }
}

void TgGateway::netReplyUsers(const QVariantMap& qm)
{
    try {
        int result = qm["Result"].toInt();
        QString repr = qm["Repr"].toString();

        TG_INFO("Update users result = " << result);

        if (result == 0) {
            // put preserved data into local struct
            users = *usersCopy;
            usersCopy = 0;
#if defined(TNET_DESKTOP)
            saveConfigUsers();
#endif
            emit requestResult(true, "Success");
        } else {
            TG_INFO("Update users failed, reason = " << repr.toLatin1().data());
            emit requestResult(false, "Fail");
        }
    } catch (TgException &e) {
        TG_ERROR("Exception e=" << e.what());
        emit requestResult(false, "Exception");
    }
}

void TgGateway::netReplyCredentials(const QVariantMap& qm)
{
    try {
        int result = qm["Result"].toInt();
        QString repr = qm["Repr"].toString();

        TG_INFO("Update id result = " << result);

        if (result == 0) {
            // put preserved data into local struct
            gwyId = *id;
            id = 0;
#if defined(TNET_DESKTOP)
            saveConfigGateway();
#endif
            emit requestResult(true, "Success");
        } else {
            TG_INFO("Update id failed, reason = " << repr.toLatin1().data());
            emit requestResult(false, "Fail");
        }
    } catch (TgException &e) {
        TG_ERROR("Exception e=" << e.what());
        emit requestResult(false, "Exception");
    }
}

void TgGateway::netReplyPowerOff(const QVariantMap& qm)
{
    try {
        int result = qm["Result"].toInt();

        TG_INFO("Power off result = " << result);

        if (result == 0) {
            emit requestResult(true, "Success");
        } else {
            emit requestResult(false, "Fail");
        }
    } catch (TgException &e) {
        TG_ERROR("Exception e=" << e.what());
        emit requestResult(false, "Exception");
    }
}

void TgGateway::netReplyMuteAlarm(const QVariantMap& qm)
{
    try {
        int result = qm["Result"].toInt();
        QVariantMap muteData = qm["Data"].toMap();

        TG_INFO("Mute alarm result = " << result);

        if (result == 0) {

            if (muteData.contains("MuteState")){
                TG_INFO("Mute state = " << muteData["MuteState"].toBool());
                muteStatus.muted = muteData["MuteState"].toBool();
            }

            if (muteData.contains("MutesRemaining")){
                TG_INFO("Mute time remaining = " << muteData["MutesRemaining"].toUInt());
                muteStatus.muteActionsTaken = muteData["MuteActions"].toUInt();
            }

            emit requestResult(true, "Success");
        } else {
            emit requestResult(false, "Fail");
        }
    } catch (TgException &e) {
        TG_ERROR("Exception e=" << e.what());
        emit requestResult(false, "Exception");
    }
}

void TgGateway::netReplyEmailImages(const QVariantMap& qm)
{
    try {
        int result = qm["Result"].toInt();

        TG_INFO("Email images result = " << result);

        if (result == 0) {
            emit requestResult(true, "Success");
        } else {
            emit requestResult(false, "Fail");
        }
    } catch (TgException &e) {
        TG_ERROR("Exception e=" << e.what());
        emit requestResult(false, "Exception");
    }
}

void TgGateway::netReplyAVConfig(const QVariantMap &qm)
{
    try {
        int result = qm["Result"].toInt();

        TG_INFO("Audio visual set config result = " << result);

        if (result == 0) {
            inout = *audioVisual;
            audioVisual = 0;
#if defined(TNET_DESKTOP)
            saveConfigAudioVisual();
#endif
            emit requestResult(true, "Success");
        } else {
            emit requestResult(false, "Fail");
        }
    } catch (TgException &e) {
        TG_ERROR("Exception e=" << e.what());
        emit requestResult(false, "Exception");
    }
}

void TgGateway::netReplyKeyConfig(const QVariantMap &qm)
{
    try {
        int result = qm["Result"].toInt();

        TG_INFO("Update key result = " << result);

        if (result == 0) {
#if defined(TNET_DESKTOP)
            saveConfigKey();
#endif
            emit requestResult(true, "Success");
        } else {
            emit requestResult(false, "Fail");
        }
    } catch (TgException &e) {
        TG_ERROR("Exception e=" << e.what());
        emit requestResult(false, "Exception");
    }
}

/**
 * @brief TgGateway::extractFinished
 * @param result
 */
void TgGateway::extractFinished(bool result)
{
    emit requestResult(result, "");
    zip = 0;
    workerThread = 0;
}



void TgGateway::printConfig()
{
    TG_DEBUG("Configuration");
    TG_DEBUG("  Owner: " << gwyId.owner);
    TG_DEBUG("  Contact: " << gwyId.contact);
    TG_DEBUG("  Install Date: " << gwyId.installdate);
    TG_DEBUG("  Lcd sw version: " << gwyId.lcdversion);
    TG_DEBUG("  Server sw version: " << gwyId.serverversion);
    TG_DEBUG("  Hardware version: " << gwyId.hwversion);
    TG_DEBUG("  Gateway name: " << gwyId.name);
    TG_DEBUG("  Serial: " << gwyId.serial);
    TG_DEBUG("  Desc 1: " << gwyId.description);
    TG_DEBUG("  Key: " << QString(hashedKey));
    TG_DEBUG("");

    TG_DEBUG("  Session name: " << session.sessionname);
    TG_DEBUG("  Session number: " << session.sessionnumber);

    TG_DEBUG("  Total sensors: " << session.totalSensors);
    TG_DEBUG("  Alarm interpretation: " << session.alarmtype);
    TG_DEBUG("  Alarm filter: " << session.triggerrate);
    TG_DEBUG("");

    for (int i=0; i<session.totalSensors; i++) {
        const Sensor_t& sensor = sensors.at(i);
        TG_DEBUG("  Sensor " << sensor.pos);
        TG_DEBUG("      Serial number: " << sensor.serial);
        TG_DEBUG("      Name: " << sensor.alias);
        TG_DEBUG("      Alarm 1: " << sensor.a1);
        TG_DEBUG("      Alarm 2: " << sensor.a2);
        TG_DEBUG("      Alarm state: " << sensor.alarmStatus);
        TG_DEBUG("      Alarm 1 triggered: " << sensor.a1trig);
        TG_DEBUG("      Alarm 2 triggered: " << sensor.a2trig);
        TG_DEBUG("      Reference: " << sensor.diffmode);
    }

    TG_DEBUG("  Vpn Network ID: " << netinfo.hamNetId);
    TG_DEBUG("  Vpn Client ID: " << netinfo.hamClientId);
    //TG_DEBUG("  Vpn Network Psk: " << netinfo.hamPsk);
    TG_DEBUG("  Vpn Ip address: " << netinfo.netip);
    TG_DEBUG("");

    TG_DEBUG("  Visual alerts: " << inout.visualalert);
    TG_DEBUG("  Audio alerts: " << inout.audioalert);
    TG_DEBUG("  Buzzer on: " << inout.buzzer);
    TG_DEBUG("  Audiomute count: " << inout.mutecount);
    TG_DEBUG("  Audiomute time: " << inout.mutetime);
    TG_DEBUG("");
}

























































/**
 * @brief TgGateway::initialise
 */
void TgGateway::testInitialise()
{
    gwyId.name = "Test Tempgard";
    gwyId.serial = "tg123456789";
    gwyId.description = "Test";

    session.seshcommit = 0;
    session.totalSensors = 10;
    session.sessionname = "";
    session.sessionnumber = "1485580976";
    session.alarmtype = aiHH;
    session.triggerrate = 3;

    for (int i=0; i<session.totalSensors; i++) {
       Sensor_t sensor;
       sensor.pos = i+1;
       sensor.alias = QString("Sensor %1").arg(i+1);
       sensor.serial = "";
       sensor.a1 = 30;
       sensor.a2 = 40;
       sensor.diffmode = 1;
       sensor.a1trig = false;
       sensor.a2trig = false;
       sensors.append(sensor);
    }
}

#if defined(TNET_DESKTOP)
/**
 * @brief TgGateway::saveIdConfig
 */
bool TgGateway::saveConfigGateway()
{
    TgJson json;

    try {

        QVariantMap tgInfo;
        QVariantMap tgRev;
        QVariantMap tgId;

        tgId["Alias"] = gwyId.name;
        tgId["Serial"] = gwyId.serial;
        tgId["D1"] = gwyId.description;

        tgInfo["Owner"] = gwyId.owner;
        tgInfo["Contact"] = gwyId.contact;
        tgInfo["InstallDate"] = gwyId.installdate;

        tgRev["Lcd"] = gwyId.lcdversion;
        tgRev["Server"] = gwyId.serverversion;
        tgRev["Hw"] = gwyId.hwversion;

        QVariantMap gateway;
        gateway["Info"] = tgInfo;
        gateway["Id"] = tgId;
        gateway["Version"] = tgRev;

        return json.toFile(gateway, QString(TNET_DEVICE_CONFIG_GATEWAY).arg(rootPath));

    } catch (TgException &e) {
        TG_ERROR("Exception e=" << e.what());
        return false;
    }
}

/**
 * @brief TgGateway::saveConfigNetwork
 * @return
 */
bool TgGateway::saveConfigNetwork()
{
    TgJson json;

    try {

        QVariantMap tgNet;

        tgNet["NetId"] = netinfo.hamNetId;
        tgNet["ClientId"] = netinfo.hamClientId;
        tgNet["NetPass"] = netinfo.hamPsk;
        tgNet["Ipv4"] = netinfo.hamIp;

        return json.toFile(tgNet, QString(TNET_DEVICE_CONFIG_HAMACHI).arg(rootPath));

    } catch (TgException &e) {
        TG_ERROR("Exception e=" << e.what());
        return false;
    }
}

/**
 * @brief TgGateway::saveConfigTemperature
 * @return
 */
bool TgGateway::saveConfigTemperature()
{
    TgJson json;

    try {
        QVariantMap tgTemp;
        QVariantMap tgSession;
        QVariantMap tgSensors;

        tgTemp["Commit"] = session.seshcommit;

        tgSession["TotalSensors"] = session.totalSensors;
        tgSession["Alias"] = session.sessionname;
        tgSession["Number"] = session.sessionnumber;
        tgSession["AlarmType"] = session.alarmtype;
        tgSession["TriggerRate"] = session.triggerrate;

        tgTemp["Session"] = tgSession;

        for (int i=0; i<session.totalSensors; i++) {
           Sensor_t sensor = sensors.at(i);
           QVariantMap tgSensor;
           tgSensor["Pos"] = sensor.pos;
           tgSensor["Alias"] = sensor.alias;
           tgSensor["Serial"] = sensor.serial;
           tgSensor["A1"] = sensor.a1;
           tgSensor["A2"] = sensor.a2;
           tgSensor["Diffmode"] = sensor.diffmode;
           tgSensor["A1trig"] = sensor.a1trig;
           tgSensor["A2trig"] = sensor.a2trig;
           tgSensors[QString("%1").arg(i+1)] = tgSensor;
        }

        tgTemp["Sensors"] = tgSensors;

        return json.toFile(tgTemp, QString(TNET_DEVICE_CONFIG_TEMPERATURE).arg(rootPath));

    } catch (TgException &e) {
        TG_ERROR("Exception e=" << e.what());
        return false;
    }
}

bool TgGateway::saveConfigUsers()
{
    try {
        TgJson json;
        QVariantMap nMap;
        QVariantMap usersMap;
        for (int i=0; i<users.count(); ++i){
            QVariantMap userMap;
            QVariantMap alertsMap;
            alertsMap.insert("LowBattery", users.at(i).alerts.lowBattery);
            alertsMap.insert("SessionNew", users.at(i).alerts.newSession);
            alertsMap.insert("SessionResume", users.at(i).alerts.resumeSession);
            alertsMap.insert("A2Alarms", users.at(i).alerts.a2Alarms);
            alertsMap.insert("A1Alarms", users.at(i).alerts.a1Alarms);
            alertsMap.insert("PowerOff", users.at(i).alerts.powerOff);
            alertsMap.insert("SessionRestart", users.at(i).alerts.restartSession);
            alertsMap.insert("SessionStop", users.at(i).alerts.stopSession);
            alertsMap.insert("PowerChange", users.at(i).alerts.powerChange);
            alertsMap.insert("IfaceChange", users.at(i).alerts.netChange);
            alertsMap.insert("PowerOn",users.at(i).alerts.powerOn);

            userMap.insert("Email", users.at(i).email);
            userMap.insert("Mobile", users.at(i).phone);
            userMap.insert("Alerts", alertsMap);

            usersMap.insert(users.at(i).name, userMap);
        }
        nMap.insert("Email", notifications.email);
        nMap.insert("Sms", notifications.sms);
        nMap.insert("Users", usersMap);

        return json.toFile(nMap, QString(TNET_DEVICE_CONFIG_NOTIFICATIONS).arg(rootPath));

    } catch (TgException &e) {
        TG_ERROR("Exception e=" << e.what());
        return false;
    }
}

bool TgGateway::saveConfigAudioVisual()
{
    TgJson json;

    try {

        QVariantMap av;

        av.insert("Audio", inout.audioalert);
        av.insert("Visual", inout.visualalert);
        av.insert("AudioMute", inout.mutecount);
        av.insert("AudioMuteTimeout", inout.mutetime);
        av.insert("Buzzer", inout.buzzer);
        TG_DEBUG("Audio = " << inout.audioalert);
        TG_DEBUG("Visual = " << inout.visualalert);
        TG_DEBUG("AudioMute = " << inout.mutecount);
        TG_DEBUG("AudioMuteTimeout = " << inout.mutetime);
        TG_DEBUG("Buzzer = " << inout.buzzer);


        return json.toFile(av, QString(TNET_DEVICE_CONFIG_AUDIOVISUAL).arg(rootPath));

    } catch (TgException &e) {
        TG_ERROR("Exception e=" << e.what());
        return false;
    }
}

bool TgGateway::saveConfigKey()
{
    QFile f(QString(TNET_DEVICE_CONFIG_KEY).arg(rootPath));

    if (!f.open(QIODevice::WriteOnly)){
        TG_ERROR("Unable to open key file for writing");
        return false;
    }

    if (f.write(hashedKey) == -1)
        TG_ERROR("Unable to write key to file");
    else
        TG_DEBUG("Success writing key");

    f.close();
    return true;
}

#endif

/**
 * @brief TgGateway::syncRecords
 * @details
 */
void TgGateway::syncRecords()
{

    // get latest file in session
    QString dirname = QString(TNET_DEVICE_SESSION_DIR).arg(rootPath, session.sessionnumber);
    QDir sessionDir(dirname);

    if (!sessionDir.exists()){
        TG_WARNING("Session dir does not exist " << dirname << ", first time sync");
        QDir d;
        d.mkdir(dirname);
        requestRecords(session.sessionnumber, "none");
        return;
    }

    // Get 1 before tdata.log
    QStringList filters;
    filters << "tdata.log.*";
    sessionDir.setNameFilters(filters);
    sessionDir.setSorting(QDir::Name);

    int totalFiles = sessionDir.entryList().count();

    for (int i=0; i<totalFiles; i++)
        TG_DEBUG("File " << i << ": " << sessionDir.entryList().at(i));

    // need method to check
    if (totalFiles == 0) {
        TG_DEBUG("No records found, request all");
        requestRecords(session.sessionnumber, "none");
    } else {
        TG_DEBUG("Most recent record file = " << sessionDir.entryList().last());
        requestRecords(session.sessionnumber, sessionDir.entryList().last());
    }

}

/**
 * @brief TgGateway::loadConfigGateway
 * @return
 */
bool TgGateway::loadConfigGateway()
{
    TgJson json;
    bool result;

    // gateway json
    QVariantMap gatewayConfig = json.parseFile(
                QString(TNET_DEVICE_CONFIG_GATEWAY).arg(rootPath),
                result).toMap();

    if (!result)
        return false;

    try {

        QVariantMap tgInfo = gatewayConfig["Info"].toMap();
        QVariantMap tgRev = gatewayConfig["Version"].toMap();
        QVariantMap tgId = gatewayConfig["Id"].toMap();

        gwyId.name = tgId["Alias"].toString();
        gwyId.serial = tgId["Serial"].toString();
        gwyId.description = tgId["D1"].toString();

        gwyId.owner = tgInfo["Owner"].toString();
        gwyId.contact = tgInfo["Contact"].toString();
        gwyId.installdate = tgInfo["InstallDate"].toString();
        gwyId.lcdversion = tgRev["Lcd"].toString();
        gwyId.serverversion = tgRev["Server"].toString();
        gwyId.hwversion = tgRev["Hw"].toString();

    } catch (TgException &e) {
        TG_ERROR("Exception e=" << e.what());
        return false;
    }

    return true;
}

/**
 * @brief TgGateway::loadConfigNetwork
 * @return
 */
bool TgGateway::loadConfigNetwork()
{
    TgJson json;
    bool result;

    // gateway json
    QVariantMap networkConfig = json.parseFile(
                QString(TNET_DEVICE_CONFIG_HAMACHI).arg(rootPath),
                result).toMap();

    if (!result)
        return false;

    try {
        netinfo.hamNetId = networkConfig["NetId"].toString();
        netinfo.hamClientId = networkConfig["ClientId"].toString();
        netinfo.hamPsk = networkConfig["NetPass"].toString();
        netinfo.hamIp = networkConfig["Ipv4"].toString();
    } catch (TgException &e) {
        TG_ERROR("Exception e=" << e.what());
        return false;
    }

    return true;
}

/**
 * @brief TgGateway::loadConfigTemperature
 * @return
 */
bool TgGateway::loadConfigTemperature()
{
    TgJson json;
    bool result;

    // gateway json
    QVariantMap tgTemp = json.parseFile(
                QString(TNET_DEVICE_CONFIG_TEMPERATURE).arg(rootPath),
                result).toMap();

    if (!result)
        return false;

    try {

        QVariantMap tgSession = tgTemp["Session"].toMap();
        QVariantMap tgSensors = tgTemp["Sensors"].toMap();

        session.seshcommit = tgTemp["Commit"].toInt();
        session.totalSensors = tgSession["TotalSensors"].toInt();
        session.sessionname = tgSession["Alias"].toString();
        session.sessionnumber = tgSession["Number"].toString();
        session.alarmtype = (TgAlarmInterpretation)tgSession["AlarmType"].toInt();
        session.triggerrate = tgSession["TriggerRate"].toInt();

        sensors.clear();
        for (int i=0; i<session.totalSensors; i++) {
           QVariantMap tgSensor = tgSensors[QString("%1").arg(i+1)].toMap();
           Sensor_t sensor;
           sensor.pos = i+1;
           sensor.alias = tgSensor["Alias"].toString();
           sensor.serial = tgSensor["Serial"].toString();
           sensor.a1 = tgSensor["A1"].toInt();
           sensor.a2 = tgSensor["A2"].toInt();
           sensor.diffmode = tgSensor["Diffmode"].toInt();
           sensor.a1trig = tgSensor["A1trig"].toBool();
           sensor.a2trig = tgSensor["A2trig"].toBool();
           if (sensor.a2trig)
               sensor.alarmStatus = daPastA2;
           else if (sensor.a1trig)
               sensor.alarmStatus = daPastA1;
           else
               sensor.alarmStatus = daNone;
           sensors.append(sensor);
        }

    } catch (TgException &e) {
        TG_ERROR("Exception e=" << e.what());
        return false;
    }

    return true;
}

bool TgGateway::loadConfigAudioVisual()
{
    TgJson json;
    bool result;

    // gateway json
    QVariantMap avmap = json.parseFile(
                QString(TNET_DEVICE_CONFIG_AUDIOVISUAL).arg(rootPath),
                result).toMap();

    if (!result){
        TG_DEBUG("Parse audio visual json config file fail");
        return false;
    }

    try {

        inout.audioalert = avmap["Audio"].toBool();
        inout.visualalert = avmap["Visual"].toBool();
        inout.buzzer = avmap["Buzzer"].toBool();
        inout.mutetime = (quint16)avmap["AudioMuteTimeout"].toUInt();
        inout.mutecount = (quint8)avmap["AudioMute"].toUInt();

    } catch (TgException &e) {
        TG_ERROR("Exception e=" << e.what());
        return false;
    }

    return true;
}

bool TgGateway::loadConfigNotifications()
{
    TgJson json;
    bool result;

    // gateway json
    QVariantMap nMap = json.parseFile(
                QString(TNET_DEVICE_CONFIG_NOTIFICATIONS).arg(rootPath),
                result).toMap();

    if (!result){
        TG_DEBUG("Parse notifications json config file fail");
        return false;
    }

    try {

        QVariantMap usersMap = nMap["Users"].toMap();
        notifications.email = nMap["Email"].toBool();
        notifications.sms = nMap["Sms"].toBool();

        users.clear();
        for (QVariantMap::const_iterator it = usersMap.begin(); it != usersMap.end(); ++it){
            UserInfo_t user;
            QVariantMap userMap = it.value().toMap();
            user.name = it.key();
            user.email = userMap["Email"].toString();
            user.phone = userMap["Mobile"].toString();
            QVariantMap userAlerts = userMap["Alerts"].toMap();
            user.alerts.lowBattery = userAlerts["LowBattery"].toBool();
            user.alerts.netChange = userAlerts["IfaceChange"].toBool();
            user.alerts.powerOn = userAlerts["PowerOn"].toBool();
            user.alerts.powerOff = userAlerts["PowerOff"].toBool();
            user.alerts.powerChange = userAlerts["PowerChange"].toBool();
            user.alerts.stopSession = userAlerts["SessionStop"].toBool();
            user.alerts.restartSession = userAlerts["SessionRestart"].toBool();
            user.alerts.resumeSession = userAlerts["SessionResume"].toBool();
            user.alerts.newSession = userAlerts["SessionNew"].toBool();
            user.alerts.a1Alarms = userAlerts["A1Alarms"].toBool();
            user.alerts.a2Alarms = userAlerts["A2Alarms"].toBool();
            users.append(user);
        }

    } catch (TgException &e) {
        TG_ERROR("Exception e=" << e.what());
        return false;
    }

    return true;
}

bool TgGateway::loadConfigKey()
{
    QFile f(QString(TNET_DEVICE_CONFIG_KEY).arg(rootPath));
    if (!f.exists()){
        TG_INFO("Key file does not exist");
        hashedKey = QByteArray();
        return false;
    }

    if (!f.open(QIODevice::ReadOnly)){
        TG_WARNING("Unable to open key file");
        return false;
    }

    hashedKey = f.readAll();

    f.close();
    return true;
}

/**
 * @brief TgGateway::connectFirstTime
 * @param ipAddr
 */
void TgGateway::connectFirstTime(const QString ipAddr)
{
    setIpAddress(ipAddr);
    requestAllConfig();
}

/**
 * @brief TgGateway::loadConfigTemperature
 * @return
 */
/*bool TgGateway::loadConfigTemperature()
{

}*/









/**
 * @brief TgGateway::on_event
 * @param eventClass
 * @param eventTopic
 * @param eventData
 * @param eventPriority
 */
void TgGateway::on_event(const QString eventClass, const QString eventTopic, const QString eventData, const TgEvStreamPriority eventPriority)
{
    TG_DEBUG("Event received:");
    TG_DEBUG("  Class = " << eventClass);
    TG_DEBUG("  Topic = " << eventTopic);
    TG_DEBUG("  Data = " << eventData);
    TG_DEBUG("  Priority = " << eventPriority);

    // status event
    if (eventClass == EVCLASS_LIVE)
        liveClassHandler(eventData);

    // temperature event
    else if (eventTopic == EVTOPIC_TEMP_NEW_DATA)
        tempClassTempHandler(eventData);

    // alarm event
    else if (eventTopic == EVTOPIC_TEMP_ALRM_A1 || eventTopic == EVTOPIC_TEMP_ALRM_A2)
        tempClassAlarmHandler(eventData);

    // power change event
    else if (eventTopic == EVTOPIC_SYS_POWERCHANGE)
        sysClassPowerChangeHandler(eventData);
}

/**
 * @brief TgGateway::liveClassHandler
 * @param s
 */
void TgGateway::liveClassHandler(const QString& eventData)
{
    try {
        TG_DEBUG("Live state event: ");

        if (!eventData.contains(',')) {
            TG_WARNING("Live stream contains no delimiters");
            return;
        }

        QStringList tokens = eventData.split(QString(","));

        if (tokens.count() == 0)
        {
            TG_WARNING("No fields in live stream");
            return;
        }

        if (tokens.count() < sfMax)
        {
            TG_WARNING("Not enough fields in live stream");
            return;
        }

        // cpu percentage use
        int cpu = tokens[sfCpu].toInt();
        if ( cpu >=0 && cpu <101 ){
            sysinfo.cpu = cpu;
            TG_DEBUG("  Cpu: " << cpu);
        } else
            TG_WARNING("    Cpu: invalid");

        // mem percentage use
        int mem = tokens[sfMem].toInt();
        if ( mem >=0 && mem <101 ){
            sysinfo.mem = mem;
            TG_DEBUG("  Mem: " << mem);
        } else
            TG_WARNING("    Mem: invalid");

        // disk percentage use
        int disk = tokens[sfDisk].toInt();
        if ( disk >=0 && disk <101 ){
            sysinfo.disk = disk;
            TG_DEBUG("  Disk: " << disk);
        }else
            TG_WARNING("    Disk: invalid");

        // ac plugged in
        if (tokens[sfAc].toInt()){
            sysinfo.ac = true;
            TG_DEBUG("  AC: yes");
        }else{
            sysinfo.ac = false;
            TG_DEBUG("  AC: no");
        }

        // usb plugged in
        if (tokens[sfUsb].toInt()){
            sysinfo.usb = true;
            TG_DEBUG("  USB: yes");
        }else{
            sysinfo.usb = false;
            TG_DEBUG("  USB: no");
        }

        // bat level
        int batlvl = tokens[sfBatLvl].toInt();
        if (batlvl > 0 && batlvl <= 100){
            sysinfo.batlvl  = (quint8)batlvl;
            TG_DEBUG("  Bat level: " << sysinfo.batlvl);
        } else
            TG_WARNING("    Bat level: invalid");

        // system uptime
        sysinfo.uptime = (quint64)tokens[sfSysUptime].toFloat();
        TG_DEBUG("  Uptime: " << sysinfo.uptime);

        // hamachi state
        if (tokens[sfHam].toInt()){
            netinfo.ham = true;
            connected = true;
            TG_DEBUG("  Vpn: yes");
        }else{
            netinfo.ham = false;
            connected = false;
            TG_WARNING(" Vpn: no");
        }

        // mute ...
        muteStatus.muted = (tokens[sfMuteOn].toInt()) ?true:false;
        if (muteStatus.muted)
            TG_DEBUG("  Muted: yes");
        else
            TG_DEBUG("  Muted: No");

        muteStatus.muteTimeRemaining = tokens[sfMuteTime].toUInt();
        TG_DEBUG("  Mute time remaining: " << muteStatus.muteTimeRemaining);

        muteStatus.muteActionsTaken = tokens[sfMuteCount].toUShort();
        TG_DEBUG("  Mute actions taken: " << muteStatus.muteActionsTaken);

        inout.mutecount = tokens[sfMuteSetting].toUShort();
        TG_DEBUG("  Mute config: " << inout.mutecount);

        // network interface ... it is string Eth,Wifi,Cell
        QString netiface = tokens[sfNetIface];
        if (netiface == QString(NETWORK_INTERFACE_ETH)){
            netinfo.netiface = ctEth;
            TG_DEBUG("  Connection interface: Ethernet");
        }else if (netiface == QString(NETWORK_INTERFACE_WIFI)){
            netinfo.netiface = ctWifi;
            TG_DEBUG("  Connection interface: Wifi");
        }else if (netiface == QString(NETWORK_INTERFACE_CELL)){
            netinfo.netiface = ctCell;
            TG_DEBUG("  Connection interface: Cellular");
        }else{
            netinfo.netiface = ctNone;
            TG_DEBUG("  Connection interface: None");
        }


        QString netip = tokens[sfNetIp];
        if (netip != ""){
            netinfo.netip = netip;
            TG_DEBUG("  LAN IP: " << netinfo.netip);
        }else
            TG_WARNING("    LAN IP: invalid");

        // session commit
        // if changed then get latest config
        quint32 seshcommit = tokens[sfSeshCommit].toInt();
        if (seshcommit > session.seshcommit) {
            TG_WARNING("Session changed, get all config");
            requestAllConfigAfterSessionChange = true;
            requestAllConfig();
            session.seshcommit = seshcommit;
            return;
        }

        // temperature controller state
        //tokens[sfTempState].toInt();

        // global alarm state
        session.alarmstate = (TgAlarmStatus)tokens[sfAlarmState].toInt();
        TG_DEBUG("  Unit alarm state: " << session.alarmstate);

        emit streamReceived(ecLive);

    } catch (TgException &e) {
        TG_ERROR("Exception e=" << e.what());
    }
}

/**
 * @brief TgGateway::tempClassTempHandler
 * @param eventData
 */
void TgGateway::tempClassTempHandler(const QString& eventData)
{
    try {

        TG_DEBUG("Sensor temperature event: ");

        if (!eventData.contains(',')) {
            TG_WARNING("No delimiter");
            return;
        }

        QStringList tokens = eventData.split(QString(","));

        if (tokens.count() == 0) {
            TG_WARNING("No fields");
            return;
        }

        if (tokens.count() != TG_CONFIG_TEMP_STREAM_SIZE(session.totalSensors) ) {
            TG_WARNING("Field count invalid expecting "<< TG_CONFIG_TEMP_STREAM_SIZE(session.totalSensors)
                       << " received " << tokens.count());
            return;
        }

        QPair<qint64, QVector<TempPoint_t> > latestPoint;
        Sensor_t *sensor;
        // date time
        latestPoint.first = QDateTime::currentDateTimeUtc().toMSecsSinceEpoch()/1000;

        // vector of temperature points
        for (int i=0; i<session.totalSensors; i++) {
            sensor = &sensors[i];

            sensor->temp = tokens[tsTemp+4*i].toFloat();
            sensor->a1 = tokens[tsA1+4*i].toInt();
            sensor->a2 = tokens[tsA2+4*i].toInt();
            sensor->alarmStatus = (TgAlarmStatus)tokens[tsAlarmState+4*i].toInt();

            // for realtime buffer and chart application
            TempPoint_t point;
            point.temp = sensor->temp;
            point.a1 = sensor->a1;
            point.a2 = sensor->a2;
            point.alarmStatus = sensor->alarmStatus;

            // push this sensor temperature in vector
            latestPoint.second.append(point);
        }

        // reached limit ?, pop oldest from the back?
        if ( streamPoints.count() == cacheSize ) {
            TG_DEBUG("Reached stream points max, poping oldest from queue");
            streamPoints.removeLast();
        }

        streamPoints.prepend(latestPoint);

        TG_DEBUG("  Stream points total = " << streamPoints.count());

        emit streamReceived(ecTemp);

    } catch (TgException &e) {
        TG_ERROR("Exception e=" << e.what());
    }
}

/**
 * @brief TgGateway::tempClassAlarmHandler
 * @param eventData
 *          format is {globalAlarmState, {pos,name,temp,a1,a2,state,triggerType}, {}, {} ...}
 *          should be atleast 1 + 7 tokens in length
 */
void TgGateway::tempClassAlarmHandler(const QString &eventData)
{
    try {

        TG_DEBUG("Sensor alarm event: ");

        if (!eventData.contains(',')) {
            TG_WARNING("No delimiter");
            return;
        }

        QStringList tokens = eventData.split(QString(","));

        if (tokens.count() == 0) {
            TG_WARNING("No fields");
            return;
        }

        if (tokens.count() < 8 ) {
            TG_WARNING("Field count invalid expecting atleast 8 "<< "received " << tokens.count());
            return;
        }

        TG_DEBUG("  Unit alarm state = " << tokens.at(0).toLatin1().data());

        int tokensProcessed = 0;
        while(tokensProcessed < tokens.count()-1 ) {

            //quint16 pos = tokens.at(1).toUShort();
            //QString alias = tokens.at(2);

            TG_DEBUG("Alarm event for sensor " << tokens.at(tokensProcessed*7+1).toLatin1().data() << ": " << tokens.at(tokensProcessed*7+2));
            TG_DEBUG("  Temperature = " << tokens.at(tokensProcessed*7+3).toLatin1().data());
            TG_DEBUG("  A1 = " << tokens.at(tokensProcessed*7+4).toLatin1().data());
            TG_DEBUG("  A2 = " << tokens.at(tokensProcessed*7+5).toLatin1().data());
            TG_DEBUG("  AlarmState =  " << tokens.at(tokensProcessed*7+6).toLatin1().data());
            TG_DEBUG("  TriggerType = " << tokens.at(tokensProcessed*7+7).toLatin1().data());
            tokensProcessed += 7;
        }

    } catch (TgException &e) {
        TG_ERROR("Exception e=" << e.what());
    }
}

/**
 * @brief TgGateway::sysClassPowerChangeHandler
 * @param eventData
 */
void TgGateway::sysClassPowerChangeHandler(const QString& eventData)
{
    try {

        TG_INFO("Power change event:");

        if (!eventData.contains(',')) {
            TG_WARNING("No delimiter");
            return;
        }

        QStringList tokens = eventData.split(QString(","));

        if (tokens.count() == 0) {
            TG_WARNING("No fields");
            return;
        }

        // should be 2 tokens with {from,to}
        if (tokens.count() != 2 ) {
            TG_WARNING("Field count invalid expecting 2 " << "received " << tokens.count());
            return;
        }

        // Battery to AC
        if (tokens.at(0) == "Battery" && tokens.at(1) == "AC Mains") {
            sysinfo.ac = true;
            sysinfo.bat = false;
            TG_INFO("  Battery to mains");
        // AC mains to battery
        } else if (tokens.at(0) == "AC Mains" && tokens.at(1) == "Battery"){
            sysinfo.ac = false;
            sysinfo.bat = true;
            TG_INFO("  Mains to battery");
        }

        // send event to gui objects
        emit streamReceived(ecSys);

    } catch (TgException &e) {
        TG_ERROR("Exception e=" << e.what());
    }
}

#include "tnetportal.h"

TgJson::TgJson()
{

}

/**
 * @brief TgJson::parseFile
 * @param fileToParse
 * @param ok
 * @return
 */
QVariant TgJson::parseFile(const QString fileToParse, bool &ok)
{
    QFile jsonFile(fileToParse);
    if (!jsonFile.exists()) {
        TG_ERROR("Json file " << fileToParse << " does not exist");
        ok = false;
        return QVariant();
    }

    if (!jsonFile.open(QIODevice::ReadOnly | QIODevice::Text)){
        TG_ERROR("Unable to open file " << fileToParse);
        ok = false;
        return QVariant();
    }

    QByteArray bytearr = jsonFile.readAll();
    return parse(bytearr, ok);
}



QVariant TgJson::parse(const QByteArray &bytearr, bool &ok)
{
#if QT_VERSION >= QT_VERSION_CHECK(5,7,0)

    QJsonParseError error;

    if (bytearr.count() == 0) {
        ok = false;
        TG_ERROR("Bytearr size = 0");
        return QVariant();
    }

    QJsonDocument jsondoc(QJsonDocument::fromJson(bytearr, &error));

    if (error.error != QJsonParseError::NoError) {
        ok = false;
        TG_ERROR("Error occurred " << error.errorString());
        return QVariant();
    }

    ok = true;
    return jsondoc.toVariant();
#elif QT_VERSION <= QT_VERSION_CHECK(4,8,7)
    QJson::Parser parser;
    QVariant result = parser.parse(bytearr, &ok);
    return result;
#endif
}

QByteArray TgJson::serialise(const QVariant &variant, bool *ok)
{
#if QT_VERSION >= QT_VERSION_CHECK(5,7,0)
    QJsonDocument json = QJsonDocument::fromVariant(variant);
    *ok = true;
    //return json.toBinaryData();
    return json.toJson();
#elif QT_VERSION <= QT_VERSION_CHECK(4,8,7)
    QJson::Serializer serializer;
    return serializer.serialize(variant, ok);
#endif
}

#if QT_VERSION >= QT_VERSION_CHECK(5,7,0)
bool TgJson::toFile(const QVariant &variant, const QString filePath)
{
  QJsonDocument json = QJsonDocument::fromVariant(variant);
  QFile jsonFile(filePath);
  if (!jsonFile.open(QIODevice::WriteOnly)) {
      TG_ERROR("Unable to open file for writing");
      return false;
  }

  if (jsonFile.write(json.toJson()) == -1){
      TG_ERROR("Unable to write json to file");
      return false;
  }

  TG_DEBUG("Success writing json");
  return true;

}
#endif

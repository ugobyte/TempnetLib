#include "tnetportal.h"

#if defined(Q_OS_LINUX)
    #include "tgow.h"
#elif defined(Q_OS_WIN)
    // #ifdef WIN
    //#include "tmex.h"
    // #elif defined(LINUX)
    // #include tgow.h // build tnetonewire for x86_
    // #endif
#endif

TnetSensorCtl::TnetSensorCtl(QObject *parent) : QObject(parent)
{
    busHandle = -1;
}

/**
 * @brief TnetSensorCtl::acquireBus
 * @details Open the com port on Desktop or /dev/ttyS2 on the temnetz unit
 * @note If running on tempnetz unit, the server has control over the bus, so need to call HTTP method to stop network
 *       and release the control port before trying to acquire the bus here.
 * @param port
 * @return
 */
bool TnetSensorCtl::acquireBus(const QString &port)
{
#if defined(Q_OS_LINUX)
    busHandle = OWAcquireBusController(port.toLatin1().data());
    if (busHandle >= 0) {
        TG_INFO("Port " << port.toLatin1().data() << " opened, handle = " << busHandle);
        return true;
    } else {
        TG_WARNING("Unable to open port " << port.toLatin1().data());
        return false;
    }
#elif defined(Q_OS_WIN)
    return true;
#endif
}

/**
 * @brief TnetSensorCtl::findSensor
 * @details Find a sensor on the 1-wire bus
 */
void TnetSensorCtl::findSensor()
{
    if (busHandle < 0) {
        TG_WARNING("Bus handle not valid");
        return;
    }

#if defined(Q_OS_LINUX)
    char * serialNumber = OWFindSensor(busHandle);
    QString serial(serialNumber);
    if (serial.count() > 0)
        emit sensorFound(true, serial);
    else
        emit sensorFound(false, "");
#elif defined(Q_OS_WIN)
    return;
#endif
}

/**
 * @brief TnetSensorCtl::readTemperature
 */
void TnetSensorCtl::readTemperature(void)
{
    if (busHandle < 0) {
        TG_WARNING("Bus handle not valid");
        return;
    }

    if (serialToRead.count() != 16) {
        TG_WARNING("Serial number not valid");
        return;
    }

#if defined(Q_OS_LINUX)
    float temperature = OWReadTemperature(busHandle, serialToRead.toLatin1().data());
    emit temperatureReady(serialToRead, temperature);
#elif defined(Q_OS_WIN)
    return;
#endif
}

/**
 * @brief TnetSensorCtl::releaseBus
 * @return
 */
bool TnetSensorCtl::releaseBus()
{
    if (busHandle < 0) {
        TG_WARNING("Bus handle not valid");
        return false;
    }

#if defined(Q_OS_LINUX)
    OWReleaseBusController(busHandle);
    busHandle = -1;
    return true;
#elif defined(Q_OS_WIN)
    return true;
#endif
}

/**
 * @brief TnetSensorCtl::setSensorForRead
 * @details Set serial number before calling readTemperature.
 * @param serial
 */
void TnetSensorCtl::setSensorForRead(QString serial)
{
    serialToRead = serial;
}

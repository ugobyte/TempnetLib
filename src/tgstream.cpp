#include "tnetportal.h"


/**
 * @brief TgStream::TgStream
 * @param parent
 * @param logger
 */
TgStream::TgStream(QObject *parent) :
    QObject(parent)
{
    setParent(parent);

    // create socket and thread
    streamSocket = new QTcpSocket(this);

    // stream socket timer to check state is connected
    streamConnectTimer = new QTimer(this);
    streamConnectTimer->setInterval(30000);
    QObject::connect(streamConnectTimer, SIGNAL(timeout()), this, SLOT(socketTimerTimeout()));
    streamConnectTimer->stop();

    // signals and slots
    QObject::connect(streamSocket, SIGNAL(connected()), this, SLOT(socketConnected()));
    QObject::connect(streamSocket, SIGNAL(disconnected()), this, SLOT(socketDisconnected()));
    QObject::connect(streamSocket, SIGNAL(readyRead()), this, SLOT(socketReadReady()));
    QObject::connect(streamSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(socketReadError(QAbstractSocket::SocketError)));
    QObject::connect(streamSocket, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SLOT(socketStateChanged(QAbstractSocket::SocketState)));

    TG_DEBUG("Constructing");
}

/**
 * @brief TgStream::~TgStream
 */
TgStream::~TgStream()
{
    delete streamConnectTimer;
    delete streamSocket;
    TG_DEBUG("Destructing");
}

/**
 * @brief TgStream::connect
 */
void TgStream::connect(QString host, quint16 port)
{
    this->host = host;
    this->port = port;
    // connect to host
    streamSocket->connectToHost(QHostAddress(this->host), this->port, QIODevice::ReadWrite);
    streamConnectTimer->start();
}

void TgStream::disconnect()
{
    streamSocket->disconnect();
}

/**
 * @brief TgStream::socketConnected
 * @details Socket connected signal emitted by tcp socket when connected to host
 */
void TgStream::socketConnected()
{
    TG_INFO("Socket connected");
    emit streamConnected();
}

/**
 * @brief TgStream::socketDisconnected
 * @details Socket disconnected signal emitted by tcp socket when disconnected from host
 */
void TgStream::socketDisconnected()
{
    TG_INFO("Socket disconnected");
    emit streamDisconnected();
}

/**
 * @brief TgStream::socketReadReady
 * @details Socket data arrived from host
 */
void TgStream::socketReadReady()
{
    TG_DEBUG("Socket data ready to read");
    QByteArray packet = streamSocket->readAll();
    streamIncomming(QString(packet));
}

/**
 * @brief TgStream::socketReadError
 * @param socketError
 */
void TgStream::socketReadError(QAbstractSocket::SocketError socketError)
{
    TG_WARNING("Socket error = " << socketError << " (" << streamSocket->errorString().toLatin1().data() << ")");
    streamSocket->abort();
    // timer should pick up that socket is disconnected and re-connect
}

/**
 * @brief TgStream::socketStateChanged
 * @param socketState
 */
void TgStream::socketStateChanged(QAbstractSocket::SocketState socketState)
{
    switch(socketState)
    {
        case QAbstractSocket::UnconnectedState:
            TG_DEBUG("Socket state unconnected");
            break;
        case QAbstractSocket::HostLookupState:
            TG_DEBUG("Socket state lookup");
            break;
        case QAbstractSocket::ConnectingState:
            TG_DEBUG("Socket state connecting");
            break;
        case QAbstractSocket::ConnectedState:
            TG_DEBUG("Socket state connected");
            break;
        case QAbstractSocket::BoundState:
            TG_DEBUG("Socket state bound");
            break;
        case QAbstractSocket::ListeningState:
            TG_DEBUG("Socket state listening");
            break;
        case QAbstractSocket::ClosingState:
            TG_DEBUG("Socket state closing");
        default:
            TG_WARNING("Socket state unknown");
            break;
    }
}

/**
 * @brief TgStream::socketTimerTimeout
 * @details Check if socket connected, if not try connect
 */
void TgStream::socketTimerTimeout()
{
    if (streamSocket->state() == QAbstractSocket::UnconnectedState) {
        TG_WARNING("Socket not connected yet");
        streamSocket->connectToHost(QHostAddress(host), port, QIODevice::ReadWrite);
    }
}




/**
 * @brief TgGateway::streamIncomming
 * @details Parse incomming data stream
 *          Data wrapped in <> SOF/EOF characters
 *          Data fields separated by : delimater
 *          Field 0 : Class
 *          Field 1 : Topic
 *          Field 2 : Priority
 *          Field 3 : Data
 *          Data field is , delimited
 * @param incommingData
 */
void TgStream::streamIncomming(QString incommingData)
{
    //TG_DEBUG("Incomming stream " << incommingData.toLatin1().data());

    // check for <>
    int dataLen = incommingData.length();
    if (incommingData[0] != '<' || incommingData[dataLen-1] != '>') {
        TG_WARNING("Incomming stream has no SOF and/or EOF");
        return;
    }

    // remove SOF,EOF
    incommingData.remove(0,1);
    incommingData.chop(1);
    //TG_DEBUG("Incomming stream less SOF/EOF " << incommingData.toLatin1().data());

    if (!incommingData.contains(':')) {
        TG_WARNING("Incomming stream contains no delimiters");
        return;
    }

    QStringList tokens = incommingData.split(QString(":"));

    if (tokens.count() == 0)
    {
        TG_WARNING("No fields in stream");
        return;
    }

    if (tokens.count() != 4)
    {
        TG_WARNING("Not enough fields in stream");
        return;
    }

    TgEvStreamPriority eventPriority;
    if (tokens[evPriority] == "0")
        eventPriority = epLow;
    else if (tokens[evPriority] == "1")
        eventPriority = epMed;
    else if (tokens[evPriority] == "2")
        eventPriority = epHigh;
    else
        eventPriority = epHigh;

    // send
    emit sendEvent(tokens[evClass], tokens[evTopic], tokens[evData], eventPriority);
}

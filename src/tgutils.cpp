#include "tnetportal.h"

/**
 * @brief utilsIntToBool
 * @param i
 * @return
 */
bool utilsIntToBool(int i){
    if (i)
        return true;
    else
        return false;
}

/**
 * @brief utilsSecondsToHms
 * @param seconds
 * @return
 */
QString utilsSecondsToHms(quint32 seconds) {
    quint32 days,hours, mins, secs;

    days = seconds / 86400;
    hours = (seconds % 86400) / 3600;
    mins = ((seconds % 86400) % 3600) / 60;
    secs = ((seconds % 86400) % 3600) / 60;

    if (days != 0)
        return QString("%1 days %2 hrs %3 mins %4 secs").arg(days).arg(hours).arg(mins).arg(secs);
    else {
        if (hours != 0)
           return QString("%1 hrs %2 mins %3 secs").arg(hours).arg(mins).arg(secs);
        else {
           if (mins != 0)
              return QString("%1 mins %2 secs").arg(mins).arg(secs);
           else
              return QString("%1 secs").arg(secs);
        }
    }
}

/**
 * @brief hashPassword
 * @param psk
 * @details md5 first 2 digits with md5 of second 2 digits to make up 256bit hash,
 *          have to use md5 because lcd touch on olimex runs on Qt 4.8 which does not have sha support
 * @return
 */
const QByteArray hashPassword(const QString &psk)
{
    QString pskright = psk.right(3);
    QCryptographicHash crypto(QCryptographicHash::Md5);
    QByteArray hash = crypto.hash(pskright.toUtf8(),QCryptographicHash::Md5) + crypto.hash(psk.toUtf8(),QCryptographicHash::Md5);
    return hash.toHex();
}

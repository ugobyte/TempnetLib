﻿
#include "tnetportal.h"
#if defined(Q_OS_WIN)
#include <QFileInfo>
#endif

/**
 * @brief TgZip::TgZip
 * @param parent
 */
TgZip::TgZip(QObject *parent, const QString archiveFile, const QString destFolder) : QObject(parent)
{
    TG_DEBUG("Constructing");
    src = archiveFile;
    dst = destFolder;
    TG_DEBUG("Archive file = " << archiveFile);
    TG_DEBUG("Destination folder = " << destFolder);
}

TgZip::~TgZip()
{
    TG_DEBUG("Destructing");
}

/**
 * @brief TgZip::extract
 * @param source
 * @param destination
 * @return
 */
void TgZip::extract(void)
{
    QFile srcFile(src);
    QDir dstDir(dst);
    if (!srcFile.exists()){
        TG_DEBUG("Source file " << src << " does not exist");
        emit extractDone(false);
        emit finished();
        return;
    }

    if (!dstDir.exists()){
        TG_DEBUG("Destination dir " << dst << " does not exist");
        emit extractDone(false);
        emit finished();
        return;
    }

#if defined(Q_OS_WIN)
    // unzip
    // stupid quirk
    //QString tmpDst = QString("%1/tmp").arg(dst);
    //QDir tmpDir(tmpDst);
    //if (!tmpDir.exists())
    //     tmpDir.mkdir(tmpDst);

    if (runExtract(src, dst)){//tmpDst)) {
        TG_ERROR("Unzip failed");
        emit extractDone(false);
        emit finished();
        return;
    }

    QFileInfo srcFileInfo(src);
    QString tarfilePath = QString("%1/%2").arg(srcFileInfo.absolutePath(), srcFileInfo.completeBaseName());
    QFile tarFile(tarfilePath);
    TG_DEBUG("Tar file = " << tarFile.fileName());

    if (!tarFile.exists()){
        TG_ERROR("Tar file does not exist");
        emit extractDone(false);
        emit finished();
        return;
    }

    // untar
    if (runExtract(tarfilePath, dst)){
        TG_ERROR("Untar failed");
        emit extractDone(false);
        emit finished();
        return;
    }

    // remove the archives
   TG_DEBUG("Removing archives");
   tarFile.remove();
   srcFile.remove();

#elif defined(Q_OS_LINUX)
    if (runExtract(src, dst)) {
        TG_ERROR("Extract failed");
        emit extractDone(false);
        emit finished();
        return;
    }

    TG_DEBUG("Removing archive");
    srcFile.remove();
#endif

    TG_INFO("Extract success");
    emit extractDone(true);
    emit finished();
}

int TgZip::runExtract(const QString source, const QString destination)
{
    QStringList args;
    QProcess process;
#if defined(Q_OS_WIN)
    args << "e" << source << QString("-o%1").arg(destination) << "-y";
    process.start("7za.exe",args);
#elif defined(Q_OS_LINUX)
    args << "-xzf" << source << "-C" << destination;
    process.start("tar",args);
#endif
    bool result = process.waitForFinished();
    TG_DEBUG("Process result = " << result);
    return 0;
}


#-------------------------------------------------
#
# Project created by QtCreator 2017-08-27T15:19:00
#
#-------------------------------------------------

QT       += core network

greaterThan(QT_MAJOR_VERSION, 4): QT += printsupport

TARGET = tnet-lib-core
TEMPLATE = lib
CONFIG += shared debug_and_release build_release
VERSION = 1.0.0
DEFINES += TNETPORTALLIB_LIBRARY

QMAKE_TARGET.arch = $$(ARCH)
QMAKE_TARGET_COMPANY = "www.tempnetz.com"
QMAKE_TARGET_COPYRIGHT = "Copyright (C) by TempnetZ"

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    tgchart.cpp \
    tgconfig.cpp \
    tggateway.cpp \
    tgstream.cpp \
    tgjson.cpp \
    tgutils.cpp \
    #tgevent.cpp \
    tgzip.cpp

HEADERS += \
    tnetportal.h

unix {
    contains(QT_ARCH, arm){
        DEFINES += "TNET_OLIMEX=1"
        LIBS += -L/usr/lib -lqjson
        message("Compiling for arm system")
    }
    contains(QT_ARCH, x86_64){
        DEFINES += "TNET_DESKTOP=1"
    }

    target.path = ../lib
    INSTALLS += target
    LIBS += -L/usr/lib -lqcustomplot
    INCLUDEPATH += /usr/include
}

win32 {
    DEFINES += "TNET_DESKTOP=1"
    INCLUDEPATH += ../../../Includes
    LIBS += -L../../../Libs -lqcustomplot2
}

CONFIG(debug, debug|release) {
    DESTDIR = build/debug_$$QT_ARCH
}
CONFIG(release, debug|release) {
    DESTDIR = build/release_$$QT_ARCH
}

OBJECTS_DIR = $$DESTDIR/.obj
MOC_DIR = $$DESTDIR/.moc
RCC_DIR = $$DESTDIR/.qrc
UI_DIR = $$DESTDIR/.ui

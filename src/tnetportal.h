#ifndef TNETPORTALLIB_H
#define TNETPORTALLIB_H

#include <QtCore/qglobal.h>

#if defined(TNETPORTALLIB_LIBRARY)
#  define TNETPORTALLIBSHARED_EXPORT Q_DECL_EXPORT
#else
#  define TNETPORTALLIBSHARED_EXPORT Q_DECL_IMPORT
#endif

#include <QWidget>
#include <QPoint>
#include <QChar>
#include <QGesture>
#include <QTimer>
#include <QString>
#include <QStringList>
#include <QMap>
#include <QtNetwork/QTcpSocket>
#include <QtNetwork/QHostAddress>
#include <QSharedPointer>
#include <QFile>
#include <QDir>
#include <QDateTime>
#include <QDate>
#include <QTextStream>
#include <QDebug>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>
#include <QVariant>
#include <QVariantMap>
#include <QSettings>
#include <QThread>
#include <QRubberBand>
#include <QProcess>
#include <QCryptographicHash>

#if QT_VERSION >= QT_VERSION_CHECK(5,7,0)
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonDocument>
#include <QException>
#elif QT_VERSION <= QT_VERSION_CHECK(4,8,7)
#include <qtconcurrentexception.h>
#include "qjson/parser.h"
#include "qjson/serializer.h"
#endif

#include "qcustomplot.h"

// Event classes
#define EVCLASS_LIVE    			"LVE"
#define EVCLASS_TEMP    			"TMP"
#define EVCLASS_SYSTEM 				"SYS"
#define EVCLASS_HAMACHI				"HAM"
#define EVCLASS_NETWORK				"NET"
#define EVCLASS_EMAIL				"EML"
#define EVCLASS_SMS 				"SMS"
#define EVCLASS_GATEWAY				"GWY"
#define EVCLASS_AUDVIS				"AUV"

// Event topics
#define EVTOPIC_TEMP_NO_CONFIG 		"000"
#define EVTOPIC_TEMP_CTRL_FAULT 	"001"
#define EVTOPIC_TEMP_ALRM_A1    	"002"
#define EVTOPIC_TEMP_ALRM_A2		"003"
#define EVTOPIC_TEMP_STOP_SESH   	"004"
#define EVTOPIC_TEMP_NEW_SESH   	"005"
#define EVTOPIC_TEMP_RESTART_SESH   "006"
#define EVTOPIC_TEMP_RESUME_SESH   	"007"
#define EVTOPIC_TEMP_NO_SENSORS     "008"
#define EVTOPIC_TEMP_NEW_DATA 		"009"

#define EVTOPIC_SYS_NO_CONFIG 		"100"
#define EVTOPIC_SYS_SHUTDOWN 		"101"
#define EVTOPIC_SYS_RESTART 		"102"
#define EVTOPIC_SYS_POWERON			"103"
#define EVTOPIC_SYS_UPDATING 		"104"
#define EVTOPIC_SYS_DISKFULL  	 	"105"
#define EVTOPIC_SYS_POWERCHANGE    	"106"
#define EVTOPIC_SYS_BATTERYLOW 		"107"

#define EVTOPIC_HAM_NO_CONFIG 		"200"
#define EVTOPIC_HAM_JOINED 			"201"
#define EVTOPIC_HAM_LEAVE 			"202"
#define EVTOPIC_HAM_OFFLINE			"203"
#define EVTOPIC_HAM_ONLINE 			"204"

#define EVTOPIC_GWY_NO_CONFIG 		"300"
#define EVTOPIC_GWY_PROVISIONED		"301"
#define EVTOPIC_GWY_FACTORY_RESET	"302"
#define EVTOPIC_GWY_NEW_ADMIN_PSK 	"303"
#define EVTOPIC_GWY_PSK_RESET 		"304"

#define EVTOPIC_AUV_NO_CONFIG		"400"
#define EVTOPIC_AUV_A1_ON			"401"
#define EVTOPIC_AUV_A1_OFF			"402"
#define EVTOPIC_AUV_A2_ON			"403"
#define EVTOPIC_AUV_A2_OFF			"404"
#define EVTOPIC_AUV_BUZZ_ON			"405"
#define EVTOPIC_AUV_BUZZ_OFF		"406"
#define EVTOPIC_AUV_OFF 			"407"
#define EVTOPIC_AUV_MUTE			"408"
#define EVTOPIC_AUV_STATE_A0		"409"
#define EVTOPIC_AUV_STATE_A1		"410"
#define EVTOPIC_AUV_STATE_A2		"411"

#define EVTOPIC_EML_NO_CONFIG		"500"
#define EVTOPIC_EML_SMTP_FAIL		"501"
#define EVTOPIC_EML_SEND_FAIL 		"502"

#define EVTOPIC_SMS_NO_CONFIG 		"600"
#define EVTOPIC_SMS_SEND_FAIL 		"601"

#define EVTOPIC_NET_NO_CONFIG 		"700"
#define EVTOPIC_NET_IFACECHANGE 	"701"
#define EVTOPIC_NET_NOINET 			"702"


// Event priorities
#define EVENT_PRIORITY_LOW			0
#define EVENT_PRIORITY_MEDIUM   	1
#define EVENT_PRIORITY_HIGH			2


#define G_UNIT_DEGREES                QChar(0260)


// plots within chart
#define TG_PLOT_TEMPERATURE                     0
#define TG_PLOT_ALARM1                          1
#define TG_PLOT_ALARM2                          2
#define TG_PLOT_REF_TEMPERATURE                 3
#define TG_PLOT_DIFF_TEMPERATURE                4


#define NETWORK_INTERFACE_NONE                  "None"
#define NETWORK_INTERFACE_ETH                   "Ethernet"
#define NETWORK_INTERFACE_WIFI                  "Wifi"
#define NETWORK_INTERFACE_CELL                  "Mobile"


#define HTTP_REQUEST_ALL_CONFIG                 "/configAll"
#define HTTP_REQUEST_WIFI_SCAN                  "/wifiScan"
#define HTTP_REQUEST_WIFI_CONNECT               "/wifiConnect"
#define HTTP_REQUEST_WIFI_DISCONNECT            "/wifiDisconnect"
#define HTTP_REQUEST_SESSION_RESUME             "/sessionResume"
#define HTTP_REQUEST_SESSION_RESTART            "/sessionRestart"
#define HTTP_REQUEST_SESSION_NEW                "/sessionNew"
#define HTTP_REQUEST_TEMP_RECORDS               "/dataRecords"
#define HTTP_REQUEST_USERS                      "/users"
#define HTTP_REQUEST_POWER_OFF                  "/powerOff"
#define HTTP_REQUEST_MUTE_ALARM                 "/audioMute"
#define HTTP_REQUEST_EMAIL_IMAGES               "/emailImages"
#define HTTP_REQUEST_AV_CONFIG				    "/audioVisual"
#define HTTP_REQUEST_CREDENTIALS                "/credentials"
#define HTTP_REQUEST_KEY                        "/key"

#define TG_CONFIG_FILE_PATH                     "%1/config/touchapp.ini"
#define TG_CONFIG_DEFAULT_LOG_PATH              "%1/log/touchapp.log"

// Windows path =  C:/users/<USER>/appData/local/Tempnetz
// Linux path = /home/<USER>/Tempnetz
// Olimex path = /home/Tempnetz
#define TNET_DEVICES_DIR                        "%1/Devices"
#define TNET_DEVICE_DIR                         "%1/Devices/%2"

// Windows arg1 = C:/users/<USER?>/appData/local/Tempnetz/Devices/<DEVICE>
// Linux arg1 = /home/Tempnetz/Devices/<DEVICE>
// Olimex arg1 = /home/Tempnetz/
#define TNET_DEVICE_CONFIG_DIR                  "%1/config"
#define TNET_DEVICE_CONFIG_GATEWAY              "%1/config/gateway.json"
#define TNET_DEVICE_CONFIG_HAMACHI              "%1/config/hamachi.json"
#define TNET_DEVICE_CONFIG_TEMPERATURE          "%1/config/temperature.json"
#define TNET_DEVICE_CONFIG_NOTIFICATIONS        "%1/config/notification.json"
#define TNET_DEVICE_CONFIG_EMAIL                "%1/config/email.json"
#define TNET_DEVICE_CONFIG_AUDIOVISUAL          "%1/config/audiovisual.json"
#define TNET_DEVICE_CONFIG_KEY                  "%1/config/.key"

#define TNET_DEVICE_ROOT_SESSION_DIR            "%1/session"
#define TNET_DEVICE_SESSION_DIR                 "%1/session/%2"
#define TNET_DEVICE_SESSION_DATA_FILE           "%1/session/%2/%3"

#define TNET_DEVICE_EVENT_DIR                   "%1/event/"
#define TNET_DEVICE_EVENT_FILE                  "%1/event/%2"

#define TNET_DEVICE_IMAGES_DIR                  "%1/images"


#define TG_CONFIG_DEFAULT_STREAM_PORT           54113
#define TG_CONFIG_DEFAULT_MANAGER_PORT          54313
#define TG_CONFIG_DEFAULT_LOG_LEVEL             lgInfo
#define TG_CONFIG_DEFAULT_CHART_STREAM_CACHE    300

#define TG_CONFIG_TEMP_STREAM_SIZE(total)       total*4

#define CelciusToFahrheit(x)                ((((x) * 9) / 5) + 32)

enum TgEvStreamFields {evClass=0, evTopic, evPriority, evData};
enum TgEvStreamClass {ecLive=0, ecTemp, ecSys, ecHam, ecNet, ecEml, ecSms, ecGwy, ecAv};
enum TgEvStreamPriority {epLow=0, epMed, epHigh };

typedef void (*StreamHandler_t)(QString &);

/* Chart */
enum TgChartViewType {cvHourly=0, cvDaily, cvWeekly, cvMonthly};
typedef struct {
    QString sensorAlias;
    quint16 sensorPos;
    QString sensorRefAlias;
    quint16 sensorRefPos;
} SensorGraphData_t;

typedef struct {
    quint8 month;
    quint16 year;
} TimeBound_t;

typedef struct {
    double x1;
    double x2;
} ViewPortRange_t;



enum TgAlarmInterpretation {aiNone=0,aiHH=1, aiHL=2, aiLL=3};
enum TgAlarmStatus {daNone=0, daPastA1=1, daPastA2=2, daCurrentA1=3, daCurrentA2=4};
enum TgDevicePowerStatus {psMains=0, psUsb=1, psBattery=2};
enum TgConnectType {ctNone=0, ctEth=1, ctWifi=2, ctCell=3};
enum TgSensorStatus {ssOnline, ssPartial, ssOffline};
enum TgWifiSecurity {wsNone, wsWep, wsWpa, wsWpa2};
enum TgWifiState {wsIdle, wsOnline, wsOffline};
enum TgIpMethod {ipDhcp, ipStatic};

enum TgLiveStreamData {sfCpu=0,
                sfMem,
                sfDisk,
                sfAc,
                sfUsb,
                sfBat,
                sfBatLvl,
                sfSysUptime,
                sfHam,
                sfMuteOn,
                sfMuteTime,
                sfMuteCount,
                sfMuteSetting,
                sfNetIface,
                sfNetIp,
                sfSeshCommit,
                sfTempState,
                sfAlarmState,
                sfMax};

enum TgTempStreamData {tsTemp=0,
                       tsA1,
                       tsA2,
                       tsAlarmState};

enum ConfigOptions {
    cfgLogPath =0,
    cfgLogLevel
};

enum TgLogLevel {lgNone=0, lgDbg=1, lgInfo=2, lgWarn=3, lgErr=4, lgCrit=5};
enum WifiPendingAction {paNone, paConnecting, paDisconnecting};


typedef struct {
    bool visualalert;
    bool audioalert;
    quint16 mutetime;
    quint8 mutecount;
    bool buzzer;
} InputOutput_t;

typedef struct {
    bool muted;
    quint16 muteActionsTaken;
    quint16 muteTimeRemaining;
} MuteStatus_t;

typedef struct {
    quint8 cpu;
    quint8 mem;
    quint8 disk;
    bool ac;
    bool usb;
    bool bat;
    quint8 batlvl;
    quint64 uptime;
} System_t;

typedef struct {
    bool email;
    bool sms;
} Notify_t;

typedef struct {
    bool lowBattery;
    bool newSession;
    bool resumeSession;
    bool restartSession;
    bool stopSession;
    bool powerChange;
    bool netChange;
    bool a1Alarms;
    bool a2Alarms;
    bool powerOn;
    bool powerOff;
} UserAlerts_t;

typedef struct {
    QString name;
    QString email;
    QString phone;
    UserAlerts_t alerts;
} UserInfo_t;

typedef struct {
    QString name;
    QString serial;
    QString description;
    QString owner;
    QString contact;
    QString installdate;
    QString serverversion;
    QString lcdversion;
    QString hwversion;
    QString key;
} GatewayId_t;

typedef struct {
    bool ham;
    QString hamClientId;
    QString hamNetId;
    QString hamIp;
    QString hamPsk;
    TgConnectType netiface;
    QString netip;
} Network_t;

typedef struct {
    QString sessionname;
    QString sessionnumber;
    quint32 seshcommit;
    bool tempstate;
    TgAlarmStatus alarmstate;
    quint8 totalSensors;
    TgAlarmInterpretation alarmtype;
    quint8 triggerrate;
} Session_t;

typedef struct {
    qint16 pos;
    QString serial;
    QString alias;
    qint16 a1;
    qint16 a2;
    bool a1trig;
    bool a2trig;
    qint16 diffmode;
    bool watch;
    float temp;
    TgAlarmStatus alarmStatus;
} Sensor_t;

typedef struct {
    qint16 a1;
    qint16 a2;
    float temp;
    TgAlarmStatus alarmStatus;
} TempPoint_t;

typedef struct {
   QString gateway;
   QString netmask;
   TgIpMethod method;
   QString address;
} TgIpv4_t;

typedef struct {
    QString ssid;
    int signal;
    bool hidden;
    TgWifiSecurity security;
    TgWifiState state;
    TgIpv4_t ipv4;
    WifiPendingAction pendingAction;
} WifiNetwork_t;

class TNETPORTALLIBSHARED_EXPORT TgConfig
{
public:
    TgConfig() {}
    ~TgConfig() {}

    void setRootPath(const QString& tgRootPath) { this->rootPath = tgRootPath; }
    const QString getRootPath(void) const { return this->rootPath; }
    void load();
    const QVariant& getOption(ConfigOptions option);
private:
    QString rootPath;
    QMap<ConfigOptions, QVariant> options;

    void loadDefaults();
};


#define TG_DEBUG(x)         qDebug() << x
#if QT_VERSION >= QT_VERSION_CHECK(5,7,0)
#define TG_INFO(x)          qInfo() << x
#elif QT_VERSION <= QT_VERSION_CHECK(4,8,7)
#define TG_INFO(x)          qDebug() << x
#endif
#define TG_WARNING(x)       qWarning() << x
#define TG_ERROR(x)         qCritical() << x
#define TG_FATAL(x)         qFatal() << x













class TNETPORTALLIBSHARED_EXPORT TgStream : public QObject
{
    Q_OBJECT
public:
    explicit TgStream(QObject *parent = 0);
    ~TgStream();

    void connect(QString host, quint16 port);
    void disconnect();

signals:

    void streamConnected();
    void streamDisconnected();
    /*void streamIncommingLive(QString &s);
    void streamIncommingTemp(QString &topic, QString &data);*/

    void sendEvent(const QString eventClass,
                   const QString eventTopic,
                   const QString eventData,
                   const TgEvStreamPriority eventPriority);
private slots:

    //  slots for stream socket signals
    void socketConnected();
    void socketDisconnected();
    void socketReadReady();
    void socketReadError(QAbstractSocket::SocketError socketError);
    void socketStateChanged(QAbstractSocket::SocketState socketState);

    // timer
    void socketTimerTimeout();

private:
    QString host;
    quint16 port;

    QTimer * streamConnectTimer;
    QTcpSocket * streamSocket;

    QMap<QString, StreamHandler_t> streamProcessMap;

    void streamIncomming(QString incommingData);
};

class TNETPORTALLIBSHARED_EXPORT TgJson
{
public:
    TgJson();
    QVariant parseFile(const QString fileToParse, bool &ok);
    QVariant parse(const QByteArray &bytearr, bool &ok);
    QByteArray serialise(const QVariant &variant, bool *ok);
#if QT_VERSION >= QT_VERSION_CHECK(5,7,0)
    bool toFile(const QVariant &variant, const QString filePath);
#endif
};



class TgZip : public QObject
{
    Q_OBJECT
public:
    explicit TgZip(QObject *parent = 0, const QString archiveFile = "", const QString destFolder = "");
    ~TgZip();

public slots:
    void extract(void);
signals:
    void finished(void);
    void extractDone(bool result);
private:
    QString src;
    QString dst;
    int runExtract(const QString source, const QString destination);
};





// 20s timeout for network request/reply
#define TNET_TIMEOUT    20000



class TNETPORTALLIBSHARED_EXPORT TgGateway : public QObject
{
    Q_OBJECT
public:
    explicit TgGateway(QObject *parent = 0,
                       const QString rpath = "");
    ~TgGateway();


    const GatewayId_t& getId()                                      const { return gwyId; }
    const InputOutput_t& getInout()                                 const { return inout; }
    const Network_t& getNetwork()                                   const { return netinfo; }
    const System_t& getSystem()                                     const { return sysinfo; }
    const Session_t& getSession()                                   const { return session; }
    const QVector<Sensor_t>& getSensors()                           const { return sensors; }
    const QVector<WifiNetwork_t>& getWifiNetworks()                 const { return networks; }
    const QList<QPair<qint64,QVector<TempPoint_t> > >& getStreamPoints()    const { return streamPoints; }
    const QVector<UserInfo_t>& getUsers()                           const { return users; }
    const MuteStatus_t& getMuteStatus()                             const { return muteStatus; }
    const QString getDevicePath(void)                               const { return rootPath; }
    const QByteArray getKey(void)                                   const { return hashedKey; }

    bool isOnline(void);

    void setCacheLimit(const int size);
    int getCacheSize(void) const;

    void enableLocalHostMode(void);
    void setIpAddress(QString ipaddr);

    bool sessionHasData(const QString& sessionNumber, bool checkConfig);
    QString getSessionName(const QString& sessionNumber);

    // load memory containers via json files
    bool loadConfigGateway(void);
    bool loadConfigNetwork(void);
    bool loadConfigTemperature(void);
    /*bool loadConfigEmail(void);*/
    bool loadConfigAudioVisual(void);
    bool loadConfigNotifications(void);
    bool loadConfigKey(void);

    void connectFirstTime(const QString ipAddr);
    void requestAllConfig(void);
    void connectStream(void);
    void disconnectStream(void);

    void requestSessionRestart(void);
    void requestSessionResume(const Session_t *newSession, const QVector<Sensor_t> *newSensors);
    void requestSessionNew(const Session_t *newSession, const QVector<Sensor_t> *newSensors);
    void requestRecords(const QString session, const QString filename);

    void requestUpdateUsers(const QVector<UserInfo_t> * users);
    void requestUpdateCredentials(const GatewayId_t * credentials);

    void requestAVConfig(const InputOutput_t * av);
    void requestMuteAlarm();
    void requestPowerOff(bool reboot);

    void requestEmailImages(const QStringList& images, const QStringList& recipients);
    void requestUpdateKey(const QString &password);

    void syncRecords(void);
    void muteAlarm(void);

    void requestWifiScan(void);
    void connectWifiNetwork(QString ssid, QString password);
    void disconnectWifiNetwork(QString ssid);
    bool isWifiScanInProgress()                                 { return wifiScanInProgress; }

signals:
    void connectionStateChange(bool connected);
    void streamReceived(TgEvStreamClass);
    void requestResult(bool, QString);

private slots:
    void streamConnected();
    void streamDisconnected();
    void on_event(const QString eventClass,
                 const QString eventTopic,
                 const QString eventData,
                 const TgEvStreamPriority eventPriority);
    void networkTimerTimeout();


    void managerReply(QNetworkReply *reply);
    void extractFinished(bool result);
private:
    GatewayId_t gwyId;
    System_t sysinfo;
    Network_t netinfo;
    InputOutput_t inout;
    Notify_t notifications;
    Session_t session;
    QVector<Sensor_t> sensors;
    QVector<WifiNetwork_t> networks;
    QVector<UserInfo_t> users;
    QByteArray hashedKey;
    MuteStatus_t muteStatus;

    QTimer networkTimer;
    TgConfig * tgconfig;
    const QString rootPath;
    TgStream * tgstream;
    QNetworkAccessManager * manager;
    TgZip * zip;
    QThread * workerThread;
    int cacheSize;
    QString ipAddress;
    QList<QPair<qint64, QVector<TempPoint_t> > > streamPoints;

    const Session_t * sessionRequest;
    const QVector<Sensor_t> * sensorsRequest;
    const QVector<UserInfo_t> * usersCopy;
    const InputOutput_t * audioVisual;
    const GatewayId_t * id;

    enum NetworkRequestCtx { getAllCfg = 0,
                             getWifiScan,
                             getTempRecords,
                             postWifiConnect,
                             postWifiDisconnect,
                             postSessionRestart,
                             postSessionResume,
                             postSessionNew,
                             postKey,
                             postUsers,
                             postId,
                             postPowerOff,
                             postMuteAlarm,
                             postAVConfig,
                             postEmailImages};

    NetworkRequestCtx ctxRequest;
    bool wifiScanInProgress;
    bool requestAllConfigAfterSessionChange;
    bool connected;

    // initialise
    void testInitialise(void);

    // dump
#if defined(TNET_DESKTOP)
    bool saveConfigGateway(void);
    bool saveConfigNetwork(void);
    bool saveConfigTemperature(void);
    bool saveConfigUsers(void);
    bool saveConfigAudioVisual(void);
    bool saveConfigKey(void);
#endif

    // http reply handlers
    void netReplyAllConfig(const QVariantMap& qm);
    void netReplyWifiScan(const QVariantMap& qm);
    void netReplyWifiConnect(const QVariantMap& qm);
    void netReplyWifiDisconnect(const QVariantMap& qm);

    void netReplySessionRestart(const QVariantMap& qm);
    void netReplySessionResume(const QVariantMap& qm);
    void netReplySessionNew(const QVariantMap& qm);

    void netReplyTempRecords(QNetworkReply *reply);

    void netReplyUsers(const QVariantMap& qm);
    void netReplyCredentials(const QVariantMap& qm);
    void netReplyPowerOff(const QVariantMap& qm);
    void netReplyMuteAlarm(const QVariantMap& qm);

    void netReplyEmailImages(const QVariantMap& qm);
    void netReplyAVConfig(const QVariantMap& qm);

    void netReplyKeyConfig(const QVariantMap& qm);



    // event handlers
    void liveClassHandler(const QString& eventData);
    void tempClassTempHandler(const QString& eventData);
    void tempClassAlarmHandler(const QString& eventData);
    void sysClassPowerChangeHandler(const QString& eventData);

    void printConfig();
};








class QRubberBand;
class QMouseEvent;
class QWidget;

// chart interaction gestures
// Pan for navigation plot through data set
// Pinch for zooming
class QGestureEvent;
class QPanGesture;
class QPinchGesture;
//class QSwipeGesture;

enum ChartItemVisibility {cvRef, cvDiff, cvA1, cvA2, cvHGrid, cvVGrid, cvLegend};

class TNETPORTALLIBSHARED_EXPORT TgChart : public QCustomPlot
{
    Q_OBJECT

public:
    TgChart(QWidget * parent = 0,
            bool degreesCelsius = true,
            QSet<ChartItemVisibility> *visibleItems = 0);
    virtual ~TgChart();

    virtual void setGateway(const TgGateway * gateway) { this->tgGateway = gateway; }
    void setSelectedPlot(int i);
    void setPlotUnits(QStringList units);
    void grabGestures(const QList<Qt::GestureType> &gestures);
    void setChartActive(bool active);
    void setZoomEnabled(bool enabled);
    void setPanEnabled(bool enabled);
    void setToolTipEnabled(bool enabled);
    virtual void clearChart(void);
    virtual void refresh(void);

    bool tempPlotIsVisible(void) { return tempPlotVisible; }
    void setTempPlotVisibility(bool visible);

    bool refPlotIsVisible(void) { return refPlotVisible; }
    void setRefPlotVisibility(bool visible);

    bool diffPlotIsVisible(void) { return diffPlotVisible; }
    void setDiffPlotVisibility(bool visible);

    bool a1PlotIsVisible(void) { return alarm1PlotVisible; }
    void setAlarm1PlotVisibility(bool visible);

    bool a2PlotIsVisible(void) { return alarm2PlotVisible; }
    void setAlarm2PlotVisibility(bool visible);

    bool hGridIsVisible(void) { return hGridVisible; }
    void setHGridVisibility(bool visible);

    bool vGridIsVisible(void) { return vGridVisible; }
    void setVGridVisibility(bool visible);

    //bool toolTipIsVisible(void) {return toolTipEnabled;}
    //void toggleTooltipVisibility(void);

    bool legendIsVisible(void) {return legendVisible; }
    void setLegendVisibility(bool visible);

    virtual void setPrintColors(bool enabled);
    bool saveAsImage(const char *format, const QString& fileName, int _width, int _height, double _scale, int _quality);

protected:
    const TgGateway * tgGateway;
    bool chartIsActive;
    bool zoomEnabled;
    bool panEnabled;
    bool toolTipEnabled;

    bool legendVisible;
    bool hGridVisible;
    bool vGridVisible;
    bool tempPlotVisible;
    bool refPlotVisible;
    bool diffPlotVisible;
    bool alarm1PlotVisible;
    bool alarm2PlotVisible;

    bool degreesInCelsius;

    quint16 sensorPos;
    quint16 sensorRefPos;
    bool event(QEvent *event);


    // 5 plots
    QCPGraph * sensorTemp;
    QCPGraph * sensorA1;
    QCPGraph * sensorA2;
    QCPGraph * refSensorTemp;
    QCPGraph * diffSensorTemp;

    // title
    QCPPlotTitle * plotTitle;

private slots:
    void mousePressEvent(QMouseEvent * event);
    void mouseMoveEvent(QMouseEvent * event);
    void mouseReleaseEvent(QMouseEvent * event);

    void dataPointHint(QMouseEvent *event);
    void chartPressed();

private:
    QRubberBand * mRubberBand;
    QPoint mOrigin;

    void panTriggered(QPanGesture *);
    void pinchTriggered(QPinchGesture *);
};



enum ChartLoadType_t {cltLatest=0, cltPrevious, cltNext, cltOldest};

class TNETPORTALLIBSHARED_EXPORT DataLoader: public QObject
{
    Q_OBJECT

public:
    explicit DataLoader(QObject *parent = 0,
                        quint16 total = 0,
                        quint16 position=0,
                        quint16 refPosition=0,
                        QString filename = "",
                        bool checkForAlarms = false,
                        bool degreesCelsius = true);
    ~DataLoader();

    void clearData();
    const QVector<double>& getTemps()                                          { return temp; }
    const QVector<double>& getDt()                                              { return dt; }
    const QVector<double>& getA1()                                              { return a1; }
    const QVector<double>& getA2()                                              { return a2; }
    const QVector<double>& getRefTemp()                                         { return refTemp; }
    const QVector<double>& getDiffTemp()                                        { return diffTemp; }
    const double& getBottomBound()                                              { return bottomBound; }
    const double& getTopBound()                                                 { return topBound; }

signals:
    void dataLoaded(bool result, bool foundAlarms);
    void finished(void);

public slots:
    void loadData(void);

private:
    QVector<double> temp;
    QVector<double> dt;
    QVector<double> a1;
    QVector<double> a2;
    QVector<double> refTemp;
    QVector<double> diffTemp;   
    quint16 totalSensors;
    quint16 sensorPos;
    quint16 sensorRefPos;
    QString fileToLoad;
    bool doAlarmSearch;
    double topBound;
    double bottomBound;
    bool degreesInCelsius;
};

class TNETPORTALLIBSHARED_EXPORT TgLogChart : public TgChart
{
    Q_OBJECT

public:
    TgLogChart(QWidget *parent=0,bool degreesCelsius=true,QSet<ChartItemVisibility> *visibleItems = 0);
    ~TgLogChart();
    bool loadSensorData(QString session,
                        quint16 totalSensors,
                        quint16 _sensorPos,
                        QString _sensorName,
                        quint16 _sensorRefPos,
                        QString _sensorRefName,
                        bool loadLatestData,
                        QString fileToLoad);
    bool loadLatest(void);
    bool loadOldest(void);
    bool panLeft(void);
    bool panRight(void);
    void zoomOut(void);
    void zoomReturn(void);
    void searchAlarms(void);
    QString getCurrentFile();
    void refresh(void);


private slots:
    void threadTimerExpired();
    void alarmSearchTimerExpired();
    void queryFinished(bool result, bool foundAlarms);

signals:
    void chartReady(bool result, bool foundAlarms); // from local to gui widget

private:
    QTimer threadTimeoutTimer;
    QTimer alarmSearchTimer;
    DataLoader * dloader;
    QThread * workerThread;
    quint32 totalPoints;
    ViewPortRange_t viewPortRange;
    ViewPortRange_t fullRange;
    QString currentFile;
    QString currentSession;
    quint16 currentFileIndex;
    QString sensorName;
    QString sensorRefName;
    quint16 sensorCount;
    quint16 totalFiles;
    QStringList logFilesEntry;
    bool alarmSearchActive;
    QString alarmSearchStartFile;
    void loadData(void);
    bool filterDataFiles(void);
    int getFileIndex(const QString& filename);
};


class TNETPORTALLIBSHARED_EXPORT TgStreamChart : public TgChart
{
    Q_OBJECT
public:
    TgStreamChart(QWidget *parent=0,bool degreesCelsius=true,QSet<ChartItemVisibility> *visibleItems = 0);
    ~TgStreamChart();
    void newSensorGraph(SensorGraphData_t graphOptions);

    void setGateway(TgGateway * gateway);
    void refresh(void);
    void setPrintColors(bool enabled);


public slots:
    void onGatewayStreamReceived(TgEvStreamClass type);
    void ticksRequestSlot(void);

private:
    bool have100Points;

};









#if QT_VERSION > QT_VERSION_CHECK(5,7,0)
class TNETPORTALLIBSHARED_EXPORT TgException : public QException
{
public:
    void raise() const { throw *this; }
    TgException *clone() const { return new TgException(*this); }
};
#elif QT_VERSION <= QT_VERSION_CHECK(4,8,7)
class TNETPORTALLIBSHARED_EXPORT TgException : public QtConcurrent::Exception
{
public:
    void raise() const { throw *this; }
    TgException *clone() const { return new TgException(*this); }
};

#endif







typedef struct {
    qulonglong dtUtc;
    QString evClass;
    QString evTopic;
    quint8 evPriority;
    QVariant evData;
} TnetEvent_t;

/*class TNETPORTALLIBSHARED_EXPORT TnetEvent : public QThread
{
    Q_OBJECT
    void run();
public:
    TnetEvent(QObject *parent=0, const QString& rpath="");
    ~TnetEvent();

    void loadEvents(void);
    const QList<TnetEvent_t>& getEvents(void);
    //const QList<TnetEvent_t>& filterClass(void);
    //const QList<TnetEvent_t>& filterTopic(void);
    //const QList<TnetEvent_t>& filterPriority(void);
   //const QList<TnetEvent_t>& search(QString search);

signals:
    void finished(bool result);

private:
    const QString rootPath;
    const TgConfig * tnetConfig;
    QList<TnetEvent_t> allEvents;
};*/







/** Utils **/

TNETPORTALLIBSHARED_EXPORT bool utilsIntToBool(int i);
TNETPORTALLIBSHARED_EXPORT QString utilsSecondsToHms(quint32 seconds);
TNETPORTALLIBSHARED_EXPORT const QByteArray hashPassword(const QString &psk);

#endif // TNETPORTALLIB_H


